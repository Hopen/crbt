/************************************************************************/
/* Name     : crbt\updatetone.h                                         */
/* Author   : Andrey Alekseev                                           */
/* Project  : CRBT                                                      */
/* Company  : Forte-CT                                                  */
/* Date     : 09 Mar 2011                                               */
/************************************************************************/
#pragma once
#include "..\ado\ADODatabase.h"
#include "logger.h"
#include "ConfigurationSettings.h"

class CUpdateToneProc: public CADOStoredProc
{
public:
	CUpdateToneProc();
	virtual ~CUpdateToneProc();

	bool AdAllParam();
	bool Connect(CADODatabase *pDB = NULL);
	bool SetAllParam(
		const int& _tone_id,
		const CString& _local_path
		);

	int Call();

private:
	singleton_auto_pointer<CSystemLog> m_pLog;

};


/******************************* eof *************************************/
// CRBT_project.cpp : Implementation of WinMain


#include "stdafx.h"
#include "resource.h"
#include "CRBT_project_i.h"
#include "../Misc/mdump.h"
#include "ConfigurationSettings.h"
#include "soapclient.h"
#include "crbtservice.h"


#include <stdio.h>

class CCRBT_projectModule : public CAtlServiceModuleT< CCRBT_projectModule, IDS_SERVICENAME >
{
public :
	DECLARE_LIBID(LIBID_CRBT_projectLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_CRBT_PROJECT, "{B614AB8D-933D-4222-81D9-578C731B0F7D}")
	HRESULT InitializeSecurity() throw()
	{
		// TODO : Call CoInitializeSecurity and provide the appropriate security settings for 
		// your service
		// Suggested - PKT Level Authentication, 
		// Impersonation Level of RPC_C_IMP_LEVEL_IDENTIFY 
		// and an appropiate Non NULL Security Descriptor.

		return S_OK;
	}
};

CCRBT_projectModule _AtlModule;
LPCTSTR c_lpszModuleName = _T("crbt.exe");



//
extern "C" int WINAPI _tWinMain(HINSTANCE /*hInstance*/, HINSTANCE /*hPrevInstance*/, 
                                LPTSTR lpCmdLine, int nShowCmd)
{
	int retval = 0;
	try
	{
		::CoInitialize(NULL);
		singleton_auto_pointer<MiniDumper> g_MiniDump;

		CCRBTSevice* pService = CCRBTSevice::GetInstance();
		if(_tcsicmp(lpCmdLine, _T("/Update")) == 0)
			pService->UpdateTone();

		//CString commandLine(lpCmdLine);
		//if(_tcsicmp(lpCmdLine, _T("/Convert")) == 0)
		//{
		//	CString test_wav(L"C:\\is3\\tones\\1_29.wav");
		//	pService->ConvertWav(test_wav);
		//}

		retval = _AtlModule.WinMain(nShowCmd);

		CCRBTSevice::ReleaseInstance();
		::CoUninitialize();
	}
	catch(CConfigurationLoadException& _exception)
	{
		return -1;
	}
	catch (...)
	{
		return -1;
	}

	return retval;
}


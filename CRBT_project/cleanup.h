/************************************************************************/
/* Name     : crbt\cleanup.h                                            */
/* Author   : Andrey Alekseev                                           */
/* Project  : CRBT                                                      */
/* Company  : Forte-CT                                                  */
/* Date     : 04 Mar 2011                                               */
/************************************************************************/
#pragma once
#include "..\ado\ADODatabase.h"
#include "logger.h"
#include "ConfigurationSettings.h"


//typedef struct Cleanup_RecordSet 
//{
//};

class CCleanupProc : public CADOStoredProc
{
public:
	CCleanupProc();
	virtual ~CCleanupProc();

	bool AdAllParam();
	bool Connect(CADODatabase *pDB = NULL);
	

	int Call();

private:
	singleton_auto_pointer<CSystemLog> m_pLog;
};



/******************************* eof *************************************/
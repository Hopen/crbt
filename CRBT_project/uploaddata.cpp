/************************************************************************/
/* Name     : crbt\uploaddata.cpp                                       */
/* Author   : Andrey Alekseev                                           */
/* Project  : CRBT                                                      */
/* Company  : Forte-CT                                                  */
/* Date     : 04 Mar 2011                                               */
/************************************************************************/
#include "stdafx.h"
#include "uploaddata.h"

CUploadDataProc::CUploadDataProc()
{

}

CUploadDataProc::~CUploadDataProc()
{

}

int CUploadDataProc::Call()
{
	if (Execute()) {
		//return GetRcParam();
		return 0;
	}
	return -1;
}

bool CUploadDataProc::AdAllParam()
{
	bool bRc = true;
	//bRc &= AdParam( _T("RETURN_VALUE"),    adInteger, adParamReturnValue, 4);
	bRc &= AdParam( _T("ToneId_in"              ),  adInteger    , adParamInput, 11);
	bRc &= AdParam( _T("toneName_in"            ),  adVarChar    , adParamInput, 255);
	bRc &= AdParam( _T("Price_in"               ),  adVarChar    , adParamInput, 255);
	bRc &= AdParam( _T("tonePreListenAddress_in"),  adVarChar    , adParamInput, 255);
	bRc &= AdParam( _T("updateTime_in"          ),  adDBTimeStamp, adParamInput, 16);
	bRc &= AdParam( _T("CatalogID_in"           ),  adInteger    , adParamInput, 11);
	bRc &= AdParam( _T("ToneCode_in"            ),  adInteger    , adParamInput, 11);
	//bRc &= AdParam( _T("localPath_in"           ),  adVarChar    , adParamInput, 255);
	return bRc;

}

bool CUploadDataProc::SetAllParam(
								  const int& _tone_id,
								  const int& _tone_code,
								  const CString& _tone_name,
								  const CString& _price,
								  const CString& _address,
								  const COleDateTime& _update_time,
								  const int& _catalogid
								  /*,
								  const CString& _llocal_path*/
								  )
{
	bool bRc = true;
	bRc &= SetParam( _T("ToneId_in"                 ), _tone_id  ); 
	bRc &= SetParam( _T("toneName_in"               ), _tone_name.GetString()); 
	bRc &= SetParam( _T("Price_in"                  ), _price.GetString()); 
	bRc &= SetParam( _T("tonePreListenAddress_in"   ), _address.GetString()); 
	bRc &= SetParam( _T("updateTime_in"             ), _variant_t(_update_time));
	bRc &= SetParam( _T("CatalogID_in"              ), _catalogid );
	bRc &= SetParam( _T("ToneCode_in"               ), _tone_code  ); 
	//bRc &= SetParam( _T("localPath_in"              ), _local_path.GetString()); 

	return bRc;
}

bool CUploadDataProc::Connect(CADODatabase *pDB /* = NULL */)
{
	try
	{
		CConfigurationSettings settings;
		return __super::Connect(L"upload_data", settings[ L"ConnectionString" ].ToStr().c_str(), pDB);
		//if (pDB) 
		//{
		//	// Use existing connection
		//	if (Initialize( L"upload_data", pDB))
		//		return AdAllParam();
		//}
		//else 
		//{
		//	// Open new connection
		//	if (Initialize( L"upload_data", settings[ L"ConnectionString" ].ToStr().c_str(), "", ""))
		//		return AdAllParam();
		//}
	}
	catch(CConfigurationLoadException& _exception)
	{
		m_pLog->LogString(LEVEL_FINE,L"Connect database string config param reading failed: %s", _exception.ToString());
	}
	catch(_com_error& err) 
	{
		m_pLog->LogString(LEVEL_INFO,L"Connect to database failed with error code: 0x%p : %s ", err.Error(), err.Description().GetBSTR());
	}
	catch(...) 
	{
		m_pLog->LogString(LEVEL_INFO,L"Unknown exception when try to connect to database");
	}
	return false;
}


/******************************* eof *************************************/
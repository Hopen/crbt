#include "sv_pipedprocess.h"

class CExecutor : public CPipedProcess
{
public:
	CExecutor(LPCTSTR _lpszCommandLine);
	~CExecutor() {};

	virtual void OnRead(LPCSTR pszData, DWORD dwSize);
};
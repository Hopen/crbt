/************************************************************************/
/* Name     : crbt\getdiff.cpp                                          */
/* Author   : Andrey Alekseev                                           */
/* Project  : CRBT                                                      */
/* Company  : Forte-CT                                                  */
/* Date     : 05 Mar 2011                                               */
/************************************************************************/
#include "stdafx.h"
#include "getdiff.h"

CGetdiffProc::CGetdiffProc()
{

}

CGetdiffProc::~CGetdiffProc()
{

}

int CGetdiffProc::Call()
{
	if (Execute()) {
		//return GetRcParam();
		return 0;
	}
	return -1;
}


bool CGetdiffProc::AdAllParam()
{
	bool bRc = true;
	//bRc &= AdParam( _T("RETURN_VALUE"),    adInteger, adParamReturnValue, 4);
	return bRc;

}

bool CGetdiffProc::Connect(CADODatabase *pDB /* = NULL */)
{
	try
	{
		CConfigurationSettings settings;
		return __super::Connect(L"get_diff", settings[ L"ConnectionString" ].ToStr().c_str(), pDB);
	}
	catch(CConfigurationLoadException& _exception)
	{
		m_pLog->LogString(LEVEL_FINE,L"Connect database string config param reading failed: %s", _exception.ToString());
	}
	catch(_com_error& err) 
	{
		m_pLog->LogString(LEVEL_INFO,L"Connect to database failed with error code: 0x%p : %s ", err.Error(), err.Description().GetBSTR());
	}
	catch(...) 
	{
		m_pLog->LogString(LEVEL_INFO,L"Unknown exception when try to connect to database");
	}
	return false;
}

bool CGetdiffProc::GetNextRecordSet(GetDiff_RecordSet_type &recSet)
{
	if (m_pRS) {
		if (m_pRS->EndOfFile != VARIANT_TRUE) {
			try {
				recSet.vIDTone             = m_pRS->Fields->GetItem( _T("ToneID"))->Value;
				recSet.vToneName           = m_pRS->Fields->GetItem( _T("toneName"))->Value;
				recSet.vPrice              = m_pRS->Fields->GetItem( _T("Price"))->Value;
				recSet.vTonePreListAddress = m_pRS->Fields->GetItem( _T("tonePreListenAddress"))->Value;
				recSet.vUpdateTime         = m_pRS->Fields->GetItem( _T("updateTime"))->Value;
				recSet.vLocalPath          = m_pRS->Fields->GetItem( _T("localPath"))->Value;
				recSet.vType               = m_pRS->Fields->GetItem( _T("status_type"))->Value;
				m_pRS->MoveNext();
				return true;
			}
			catch(_com_error& err) 
			{
				m_pLog->LogString(LEVEL_INFO,L"Get_diff recordset failed with error code: 0x%p : %s ", err.Error(), err.Description().GetBSTR());
			}
			catch(...) 
			{
				m_pLog->LogString(LEVEL_INFO,L"Unknown exception when try to get GetDiff recordset");
			}
		}
	}
	return false;
}
/************************************************************************/
/* Name     : crbt\soapclient.h                                         */
/* Author   : Andrey Alekseev                                           */
/* Project  : CRBT                                                      */
/* Company  : Forte-CT                                                  */
/* Date     : 21 Feb 2011                                               */
/************************************************************************/
#pragma once
#include "singleton.h"
#include "logger.h"

#import "Winhttp.dll" no_namespace named_guids


class CSoapClientBase: public singleton <CSoapClientBase>
{
private:
	CSoapClientBase();

private:
	friend class singleton<CSoapClientBase>;

protected:
	CComPtr<IWinHttpRequest> pHTTPRequest;
	void	SetRequestHeader(LPCWSTR _lpszHeader, LPCWSTR _lpszText);
public:
	HRESULT PrepareHTTPRequest(const _bstr_t& _bstrUrl);
	HRESULT SendSoapMessage(const _bstr_t& _bstrSoapAction, const _bstr_t& _bstrSoapText, LONG _timeout, LONG* _lResult, _bstr_t& _bstrText);
	
};

class CCRBTClient//: public CSoapClientBase
{
public:
	CCRBTClient();

	bool GetNewCRBT(CString& _response, const CString& sCatalogID);
private:
	singleton_auto_pointer<CSystemLog>		m_pLog;
	CSoapClientBase * m_pClientBase;

	int     m_dwTimeout;
	CString m_SoapAction;
	CString m_strRPPUrl;
	CString m_portalAccount;
	CString m_portalPwd;
	CString m_startRecordNum;
	CString m_endRecordNum;
	bool    m_EnableRecordNum;
};

/******************************* eof *************************************/
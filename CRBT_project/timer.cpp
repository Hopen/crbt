/************************************************************************/
/* Name     : crbt\timer.cpp                                            */
/* Author   : Andrey Alekseev                                           */
/* Project  : CRBT                                                      */
/* Company  : Forte-CT                                                  */
/* Date     : 11 Mar 2011                                               */
/************************************************************************/
#include "stdafx.h";
#include "timer.h"

CTimer::CTimer(boost::asio::io_service& io, SimpleCallbackFunc _callback, const int& TimeToFirstExpire/*, void *param*/)
	:m_timer1(io,boost::posix_time::seconds(TimeToFirstExpire)),
	 m_CallBack(_callback)/*,
	 m_param(param)*/
{
	m_bCanceled = false;
	m_timer1.async_wait(boost::bind(&CTimer::Timer1Expired, this));
}
CTimer::~CTimer()
{
	Cancel();
}


void CTimer::Timer1Expired()
{
	if (IsCanceled())
		return;
	m_CallBack(/*m_param*/);
	m_timer1.expires_at(m_timer1.expires_at() + tUpdate);
	m_timer1.async_wait(boost::bind(&CTimer::Timer1Expired, this));
}
void CTimer::Cancel()
{
	m_bCanceled = true;
	m_timer1.cancel();
}

/******************************* eof *************************************/
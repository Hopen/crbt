

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0500 */
/* at Wed Apr 27 17:49:59 2011
 */
/* Compiler settings for .\CRBT_project.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __CRBT_project_i_h__
#define __CRBT_project_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __ICRBT_FWD_DEFINED__
#define __ICRBT_FWD_DEFINED__
typedef interface ICRBT ICRBT;
#endif 	/* __ICRBT_FWD_DEFINED__ */


#ifndef __CRBT_FWD_DEFINED__
#define __CRBT_FWD_DEFINED__

#ifdef __cplusplus
typedef class CRBT CRBT;
#else
typedef struct CRBT CRBT;
#endif /* __cplusplus */

#endif 	/* __CRBT_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __ICRBT_INTERFACE_DEFINED__
#define __ICRBT_INTERFACE_DEFINED__

/* interface ICRBT */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ICRBT;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("B5505EC2-F11D-419C-A7E8-C887BFF65E6A")
    ICRBT : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Update( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Update2( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICRBTVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICRBT * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICRBT * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICRBT * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ICRBT * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ICRBT * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ICRBT * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICRBT * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Update )( 
            ICRBT * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Update2 )( 
            ICRBT * This);
        
        END_INTERFACE
    } ICRBTVtbl;

    interface ICRBT
    {
        CONST_VTBL struct ICRBTVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICRBT_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ICRBT_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ICRBT_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ICRBT_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ICRBT_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ICRBT_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ICRBT_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ICRBT_Update(This)	\
    ( (This)->lpVtbl -> Update(This) ) 

#define ICRBT_Update2(This)	\
    ( (This)->lpVtbl -> Update2(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ICRBT_INTERFACE_DEFINED__ */



#ifndef __CRBT_projectLib_LIBRARY_DEFINED__
#define __CRBT_projectLib_LIBRARY_DEFINED__

/* library CRBT_projectLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_CRBT_projectLib;

EXTERN_C const CLSID CLSID_CRBT;

#ifdef __cplusplus

class DECLSPEC_UUID("883FD0B3-C947-4E6C-8A6D-C672341CF086")
CRBT;
#endif
#endif /* __CRBT_projectLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif



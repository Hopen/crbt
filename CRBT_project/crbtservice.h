/************************************************************************/
/* Name     : crbt\crbtsevice.h                                         */
/* Author   : Andrey Alekseev                                           */
/* Project  : CRBT                                                      */
/* Company  : Forte-CT                                                  */
/* Date     : 24 Feb 2011                                               */
/************************************************************************/
#pragma once
#include <map>
#include <algorithm>
#include <boost/thread.hpp>
#include "xml.h"
#include "soapclient.h"
#include "..\ado\ADODatabase.h"
#include "TransportManager.h"
#include "utils.h"
//#import "MsXml6.dll"
#include "timer.h"

const UINT charset_utf_8    = 1200;

class CCRBTSevice: public singleton <CCRBTSevice>
{
private:

	class CRBTInfo
	{
	public:
		CRBTInfo(MSXML2::IXMLDOMNodePtr plDomNode, const CString& _replaceable, const CString& _replaced, const CString& sCatalogID)
		{
			Load(plDomNode,_replaceable,_replaced,Utils::toNum<int>(sCatalogID.GetString()));
		}
		CRBTInfo(MSXML2::IXMLDOMNodePtr plDomNode, const CString& _replaceable, const CString& _replaced, const int& iCatalogID)
		{
			Load(plDomNode,_replaceable,_replaced,iCatalogID);
		}
	private:
		CString m_toneID;
		CString m_toneCode;
		CString m_toneName;
		CString m_price;
		CString m_tonePreListenAddress;
		CString m_updateTime;
		int     m_catalogid;

		CString SelectSingleNode(wchar_t* sQuery, MSXML2::IXMLDOMNodePtr plDomNode)
		{
			CString  retVal = L"";
			MSXML2::IXMLDOMNodePtr	Node;
			if (plDomNode)
			{
				Node = plDomNode->selectSingleNode(sQuery);
				if (Node)
					retVal = bstr_t(Node->text).GetBSTR();
			}
			if (Node)
				Node.Release();
			return retVal;
		}

		void Load (MSXML2::IXMLDOMNodePtr plDomNode, const CString& _replaceable, const CString& _replaced, const int& iCatalogID)
		{
			m_toneID                = SelectSingleNode(L"toneID"               , plDomNode);
			m_toneCode              = SelectSingleNode(L"toneCode"             , plDomNode);
			m_toneName              = SelectSingleNode(L"toneName"             , plDomNode);
			m_price                 = SelectSingleNode(L"price"                , plDomNode);
			m_tonePreListenAddress  = SelectSingleNode(L"tonePreListenAddress" , plDomNode);
			m_updateTime            = SelectSingleNode(L"updateTime"           , plDomNode);

			m_tonePreListenAddress.Replace(_replaceable,_replaced);
			m_catalogid             = iCatalogID;
		}
	public:
		int GetToneID()const
		{
			return Utils::toNum<int>(m_toneID.GetString());
		}
		int GetToneCode()const
		{
			return Utils::toNum<int>(m_toneCode.GetString());
		}
		inline CString GetToneName()const{return m_toneName;}
		inline CString GetPrice()const{return m_price;}
		inline CString GetToneAddress()const{return m_tonePreListenAddress;}
		inline COleDateTime GetUpdateTime()const
		{
			COleDateTime now;
			DWORD dwYear,dwMon,dwDay,dwHour,dwMin,dwSec;
			std::wstring s(m_updateTime);
			if (swscanf(m_updateTime.GetString(), L"%u-%u-%u %u:%u:%u", &dwYear, &dwMon, &dwDay, &dwHour, &dwMin, &dwSec)!=6)
				throw std::wstring(L"Invalid time value '" + m_updateTime + L"'");
			//now.SetDate(dwYear,dwMon,dwDay);
			//now.SetTime(dwHour,dwMin,dwSec);
			now.SetDateTime(dwYear,dwMon,dwDay,dwHour,dwMin,dwSec);
			//int day = now.GetDay();
			//int mon = now.GetMonth();
			//int yaer = now.GetYear();
			//int hour = now.GetHour();
			//int min = now.GetMinute();
			//int sec =now.GetSecond();
			return now;
		}
		int GetCatalogID()const {return m_catalogid;}

		CString Info()const
		{
			CString _info;
			_info.Format(L" toneID: %s, toneCode: %s, toneName: %s, price: %s, tonePreListenAddress: %s, UpdateTime: %s, catalogID: %i ", 
				m_toneID,
				m_toneCode,
				m_toneName,
				m_price,
				m_tonePreListenAddress,
				m_updateTime,
				m_catalogid);
			return _info;
		}

	};

	struct CRBTDownloadInfo
	{
		CRBTDownloadInfo(){}
		CRBTDownloadInfo(/*const CString& _toneid*/const int& _toneid, const CString& _localpath)
		{
			toneID    = _toneid;
			localpath = _localpath;
		}
	private:
		int toneID;
		CString localpath;
	public:
		int GetToneID()const
		{
			//return Utils::toNum<int>(toneID.GetString());
			return toneID;
		}
		CString GetLocalPath()const{return localpath;}
	};

	typedef std::map<long, CRBTDownloadInfo> TransferFiles;

	friend class singleton<CCRBTSevice>;
	CCRBTSevice();
	~CCRBTSevice();

public:
	void SetTimer(const CString& _update_time);
	static void UpdateTone();
private:
	static boost::shared_ptr<CCRBTClient> m_soapclient;
	//static boost::shared_ptr<CADODatabase>m_database;
	static singleton_auto_pointer<CSystemLog>		m_pLog;
	static boost::shared_ptr<CTransportManager> m_transport;
	ATL::CCriticalSection			 m_access;
	CTransportManager::Connector					m_successConnector;
	CTransportManager::Connector					m_failureConnector;
	singleton_auto_pointer<CMonitorThread>	m_pMonitor;
	boost::shared_ptr<CTimer> m_timer;
	//CTimer *m_timer;
	boost::asio::io_service m_io;

	static TransferFiles m_downloading;
	int	m_nDefaultBitsPerSample;
	int	m_nDefaultSamplesPerSecond;
	CString m_sConvertFormat;
	boost::shared_ptr<boost::thread>m_pThread;
	CString m_3gpConverterFilePath;


	static HRESULT Load(const std::wstring &sResource,  const std::wstring &sSchemaFile, BOOL bFile, MSXML2::IXMLDOMNodePtr& _doc);
	static CString GetTempWavName(const CString& _temp_folder, const CString& _fileurl);
	//static void DecodeXML(CString &xml);

	void OnDownloadSuccess(int _jobId);
	void OnDownloadFailure(int _jobId);

	void SubscribeTransportMessages();
public:
	void ConvertWav(CString& _name);
};


/******************************* eof *************************************/
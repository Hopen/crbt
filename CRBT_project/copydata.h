/************************************************************************/
/* Name     : crbt\copydata.h                                           */
/* Author   : Andrey Alekseev                                           */
/* Project  : CRBT                                                      */
/* Company  : Forte-CT                                                  */
/* Date     : 05 Mar 2011                                               */
/************************************************************************/
#pragma once
#include "..\ado\ADODatabase.h"
#include "logger.h"
#include "ConfigurationSettings.h"


class CCopydataProc : public CADOStoredProc
{
public:
	CCopydataProc();
	virtual ~CCopydataProc();

	bool AdAllParam();
	bool Connect(CADODatabase *pDB = NULL);

	int Call();

private:
	singleton_auto_pointer<CSystemLog> m_pLog;
};

/******************************* eof *************************************/
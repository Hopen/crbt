/************************************************************************/
/* Name     : crbt\getdiff.h                                            */
/* Author   : Andrey Alekseev                                           */
/* Project  : CRBT                                                      */
/* Company  : Forte-CT                                                  */
/* Date     : 05 Mar 2011                                               */
/************************************************************************/
#pragma once
#include "..\ado\ADODatabase.h"
#include "logger.h"
#include "ConfigurationSettings.h"

typedef struct GetDiff_RecordSet 
{
	_variant_t vIDTone;
	_variant_t vToneName;
	_variant_t vPrice;
	_variant_t vTonePreListAddress;
	_variant_t vUpdateTime;
	_variant_t vLocalPath;
	_variant_t vType;

} GetDiff_RecordSet_type;

class CGetdiffProc : public CADOStoredProc
{
public:
	CGetdiffProc();
	virtual ~CGetdiffProc();

	bool AdAllParam();
	bool Connect(CADODatabase *pDB = NULL);

	int Call();

	bool GetNextRecordSet(GetDiff_RecordSet_type &recSet);
private:
	singleton_auto_pointer<CSystemLog> m_pLog;
};

/******************************* eof *************************************/
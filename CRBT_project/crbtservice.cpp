/************************************************************************/
/* Name     : crbt\crbtsevice.cpp                                       */
/* Author   : Andrey Alekseev                                           */
/* Project  : CRBT                                                      */
/* Company  : Forte-CT                                                  */
/* Date     : 24 Feb 2011                                               */
/************************************************************************/


#include "stdafx.h"
#include <vector>
//#include <mlang.h>
#include <fstream>
#include <sstream>

#include "EvtModel.h"
#include "crbtservice.h"
#include "cleanup.h"
#include "uploaddata.h"
#include "getdiff.h"
#include "copydata.h"
#include "updatetone.h"
#include "ConvertAction.h"
#include "..\Dialogic\Wave.h"

#include <boost/asio.hpp>
#include "..\Common\Misc\stringfunctions.h"
#include "..\Common\Misc\ParsedFileName.h"

#include "executor.h"


CCRBTSevice::TransferFiles           CCRBTSevice::m_downloading;
singleton_auto_pointer<CSystemLog>	 CCRBTSevice::m_pLog;
//boost::shared_ptr<CADODatabase>          CCRBTSevice::m_database;
boost::shared_ptr<CCRBTClient>           CCRBTSevice::m_soapclient;
boost::shared_ptr<CTransportManager>     CCRBTSevice::m_transport;

const int RAW_FORMAT = -1;
const int F3GP_FORMAT = -2;

//void CCRBTSevice::DecodeXML(CString &xml)
//{
//	IMultiLanguage2 *mlang;
//	DetectEncodingInfo info = {0};
//	MIMECPINFO codepageinfo;
//	int length = 0, cnt = 1;
//	unsigned int len =1;
//
//	if(xml)
//		length = xml.GetLength();
//
//	HRESULT hr = S_OK;
//
//	hr = CoCreateInstance(CLSID_CMultiLanguage, NULL, CLSCTX_INPROC_SERVER, IID_IMultiLanguage2,(void **)&mlang);
//
//	if(SUCCEEDED(hr))
//		hr = mlang->DetectInputCodepage(0,0, (CHAR*)xml.GetString(), &length, &info, &cnt);
//
//	if(SUCCEEDED(hr))
//		hr = mlang->GetCodePageInfo(info.nCodePage, info.nLangID, &codepageinfo);
//
//
//	//CString ss1;
//	//if((SUCCEEDED(hr))&& (codepageinfo.uiFamilyCodePage == charset_utf_8))
//	//{
//	//	// decoding
//	//	int row_size = xml.GetLength();
//	//	WCHAR * ws = new WCHAR [row_size + 1];
//	//	if (MultiByteToWideChar(CP_UTF8,0,xml.GetString(),row_size + 1, ws, row_size + 1))
//	//		xml = (wchar_t*)ws;
//	//	delete [] ws;
//	//	ws = NULL;
//	//}
//	mlang->Release();
//}

class CSmartADODataBase
{
public:
	CSmartADODataBase::CSmartADODataBase()
	{
		CConfigurationSettings settings;
		// Initiate database
		try
		{
			m_database = boost::shared_ptr<CADODatabase>(new CADODatabase());
			m_database->Open(settings[ L"ConnectionString" ].ToStr().c_str(), "", "", "", adUseClient);
		}
		catch(CConfigurationLoadException& _exception)
		{
			m_pLog->LogString(LEVEL_SEVERE,L"Connect database string config param reading failed: %s", _exception.ToString());
			throw;
		}
		catch(_com_error& err) 
		{
			m_pLog->LogString(LEVEL_SEVERE,L"Connect to database failed with error code: 0x%p : %s ", err.Error(), err.Description().GetBSTR());
			throw;
		}
		catch(...) 
		{
			m_pLog->LogString(LEVEL_SEVERE,L"Unknown exception when try to connect to database");
			throw;
		}
	}
	CSmartADODataBase::~CSmartADODataBase()
	{
		m_database->Close();
	}

	CADODatabase* get()
	{
		return m_database.get();
	}
	bool Execute(LPCTSTR lpstrExec)
	{
		return m_database->Execute(lpstrExec);
	}
private:
	boost::shared_ptr<CADODatabase>m_database;
	singleton_auto_pointer<CSystemLog>		m_pLog;
};

HRESULT CCRBTSevice::Load(const std::wstring &sResource,  const std::wstring &sSchemaFile, BOOL bFile, MSXML2::IXMLDOMNodePtr& _doc)
{
	MSXML2::IXMLDOMDocument2Ptr xmlDoc;
	MSXML2::IXMLDOMSchemaCollection2Ptr xmlSch;

	VARIANT_BOOL isSuccessful = VARIANT_FALSE;
	HRESULT hr = E_FAIL;

	m_pLog->LogString(LEVEL_INFO, L"XML CoCreateInstance(__uuidof(MSXML2::DOMDocument60))");
	if (FAILED(hr = xmlDoc.CreateInstance(__uuidof(MSXML2::DOMDocument60))))
	{
		m_pLog->LogString(LEVEL_INFO, L"CoCreateInstance(__uuidof(MSXML2::DOMDocument60)) failed (0x%x)", hr);
		return hr;
	}

	m_pLog->LogString(LEVEL_INFO, L"load XML document");
	isSuccessful = bFile ? 
		xmlDoc->load(_bstr_t(sResource.c_str())) : 
	xmlDoc->loadXML(_bstr_t(sResource.c_str()));

	if (isSuccessful != VARIANT_TRUE)
	{
		MSXML2::IXMLDOMParseErrorPtr err = xmlDoc->parseError;

		m_pLog->LogString(LEVEL_INFO, L"Cannot load XML document. Line %d, char %d, reason \"%s\"", 
			err->line, err->linepos, (LPCWSTR)err->reason);
		return E_FAIL;
	}

	if (!sSchemaFile.empty())
	{
		// decide, which namespace we should use
		int pos = -1;
		std::wstring _file(sSchemaFile),schema_namespace;
		if ((pos = _file.rfind('\\')) != -1)
		{
			_file.erase(0,pos+1);
		}
		if ((pos = _file.rfind('.')) != -1)
		{
			WCHAR file_name[MAX_PATH];ZeroMemory(file_name,sizeof(file_name));
			_file._Copy_s(file_name,MAX_PATH,pos);
			schema_namespace=std::wstring(L"x-schema:")+file_name;

		}
		// xsd
		MSXML2::IXMLDOMDocument2Ptr pXSDDoc;
		m_pLog->LogString(LEVEL_INFO, L"XSD CoCreateInstance(__uuidof(MSXML2::XMLSchemaCache60))");
		if (FAILED(hr = pXSDDoc.CreateInstance(__uuidof(MSXML2::DOMDocument60)))) 
		{
			m_pLog->LogString(LEVEL_INFO, L"XSD CoCreateInstance(__uuidof(MSXML2::DOMDocument60)) failed (0x%x)", hr);
			return hr;
		}
		pXSDDoc->async = false; 
		pXSDDoc->validateOnParse = true;  
		m_pLog->LogString(LEVEL_INFO, L"load XSD document");
		if(pXSDDoc->load(_variant_t(sSchemaFile.c_str()))!=VARIANT_TRUE)
		{
			MSXML2::IXMLDOMParseErrorPtr err = xmlDoc->parseError;

			m_pLog->LogString(LEVEL_INFO, L"Cannot load XSD document. Line %d, char %d, reason \"%s\"", 
				err->line, err->linepos, (LPCWSTR)err->reason);
		}


		// schema
		m_pLog->LogString(LEVEL_INFO, L"Schema CoCreateInstance(__uuidof(MSXML2::XMLSchemaCache60))");
		if (FAILED(hr = xmlSch.CreateInstance(__uuidof(MSXML2::XMLSchemaCache60))))
		{
			m_pLog->LogString(LEVEL_INFO, L"CoCreateInstance(__uuidof(MSXML2::XMLSchemaCache60)) failed (0x%x)", hr);
			return hr;
		}

		// 
		m_pLog->LogString(LEVEL_INFO, L"get schema interface");
		xmlDoc->schemas = xmlSch.GetInterfacePtr();

		m_pLog->LogString(LEVEL_INFO, L"add xsd to schema");

		//hr = xmlSch->add(_bstr_t(L"x-schema:ccxml"),  pXSDDoc.GetInterfacePtr());
		hr = xmlSch->add(schema_namespace.c_str(),  pXSDDoc.GetInterfacePtr());



		//CCacheInterfaceModule::m_pLog->Log(__FUNCTIONW__, L"get interface");
		m_pLog->LogString(LEVEL_INFO, L"Validating DOM");

		MSXML2::IXMLDOMParseError2Ptr pError =xmlDoc->validate();
		if (pError->errorCode != 0) 
		{
			//MSXML2::IXMLDOMParseErrorPtr err = xmlDoc->parseError;

			m_pLog->LogString(LEVEL_INFO, L"Cannot load XML document. Line %d, char %d, reason \"%s\"", 
				pError->line, pError->linepos, (LPCWSTR)pError->reason);
			return hr;
		}

	}

	xmlDoc->setProperty(L"SelectionLanguage", L"XPath");
	std::wstring namesp = L"xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' ";
	xmlDoc->setProperty(L"SelectionNamespaces",namesp.c_str());


	_doc = xmlDoc->documentElement;
	return hr;

}


CCRBTSevice::CCRBTSevice()//:m_timer(NULL)
{
	m_soapclient = boost::shared_ptr<CCRBTClient>(new CCRBTClient());
	SubscribeTransportMessages();
	CConfigurationSettings settings;
	// Initiate database
	//try
	//{
	//	m_database = boost::shared_ptr<CADODatabase>(new CADODatabase());
	//	m_database->Open(settings[ L"ConnectionString" ].ToStr().c_str(), "", "", "", adUseClient);
	//}
	//catch(CConfigurationLoadException& _exception)
	//{
	//	m_pLog->LogString(LEVEL_SEVERE,L"Connect database string config param reading failed: %s", _exception.ToString());
	//	throw;
	//}
	//catch(_com_error& err) 
	//{
	//	m_pLog->LogString(LEVEL_SEVERE,L"Connect to database failed with error code: 0x%p : %s ", err.Error(), err.Description().GetBSTR());
	//	throw;
	//}
	//catch(...) 
	//{
	//	m_pLog->LogString(LEVEL_SEVERE,L"Unknown exception when try to connect to database");
	//	throw;
	//}

	// Set update timer
	try
	{
		SetTimer(settings[L"UpdateStartTime"].ToString());
	}
	catch (CString& strError)
	{
		m_pLog->LogString(LEVEL_SEVERE,L"Set update timer failed with error : %s ", strError);
		throw;
	}
	catch(CConfigurationLoadException& _exception)
	{
		m_pLog->LogString(LEVEL_SEVERE,L"Cannot read UpdateStartTime config param : %s", _exception.ToString());
		throw;
	}
	catch(...) 
	{
		m_pLog->LogString(LEVEL_SEVERE,L"Unknown exception when try to start update timer");
		throw;
	}

	try
	{
		m_nDefaultSamplesPerSecond = settings[L"SamplesPerSecond"].ToInt();
	}
	catch(...)
	{
		m_nDefaultSamplesPerSecond = 11025;
	}

	try
	{
		m_nDefaultBitsPerSample	= settings[L"BitsPerSample"].ToInt();
	}
	catch(...)
	{
		m_nDefaultBitsPerSample = 8;
	}

	try
	{
		m_sConvertFormat = settings[L"WavFormat"].ToString();
	}
	catch(...)
	{
		m_sConvertFormat = L"PCM";
	}

	CParsedFileName pfn(CProcessHelper::GetCurrentModuleName());
	m_3gpConverterFilePath = pfn.FullFolderName() + L"3gpConverter\\convert.bat";
	std::ifstream is(m_3gpConverterFilePath, std::ios::in | std::ios::binary);
	if (!is.is_open())
	{
		m_pLog->LogString(LEVEL_SEVERE,L"Could not find 3gp converter by path name : %s ", m_3gpConverterFilePath);
		m_3gpConverterFilePath.Empty();
	}
	//while (is.read(buf, sizeof(buf)).gcount() > 0)

	//m_3gpConverterFilePath

}

void CCRBTSevice::SubscribeTransportMessages()
{
	try
	{
		m_transport = boost::shared_ptr<CTransportManager>( new CTransportManager(m_access));
		m_successConnector = m_transport->AddJobCompleteHandler(boost::bind(&CCRBTSevice::OnDownloadSuccess, this, _1));
		m_failureConnector = m_transport->AddJobErrorHandler(boost::bind(&CCRBTSevice::OnDownloadFailure, this, _1));
		m_pMonitor->Start();
	}
	catch(...)
	{
		m_pLog->LogString(LEVEL_SEVERE,L"Unknown exception when subscribing to TranportManager");
	}
}

CCRBTSevice::~CCRBTSevice()
{
	m_timer->Cancel();
	//m_io.stop();
	m_pThread->join();
	//delete m_timer;
	m_successConnector.disconnect();
	m_failureConnector.disconnect();
	m_pMonitor->Stop();
	//m_timer->Cancel();
	//m_pThread->interrupt();

}

void CCRBTSevice::SetTimer(const CString& _update_time)
{
	DWORD dwHour,dwMin,dwSec;
	if (swscanf(_update_time.GetString(), L"%u:%u:%u", &dwHour, &dwMin, &dwSec)!=3)
		throw std::wstring(L"Invalid timer format '" + _update_time + L"', use 'hh:mm:ss'");
	
	time_t rawtime;
	tm * timeinfo1,
		* timeinfo2;

	time ( &rawtime );
	timeinfo1 = new tm;
	memcpy(timeinfo1,localtime ( &rawtime ),sizeof(tm));
	timeinfo2 = new tm;
	memcpy(timeinfo2,localtime ( &rawtime ),sizeof(tm));

	timeinfo2->tm_hour = dwHour;
	timeinfo2->tm_min  = dwMin;
	timeinfo2->tm_sec  = dwSec;

	//if (timeinfo2->tm_hour - timeinfo1->tm_hour < 0)
	//	++timeinfo2->tm_mday;

	//if (timeinfo2->tm_hour - timeinfo1->tm_hour == 0 && 
	//	timeinfo2->tm_min  - timeinfo1->tm_min   < 0)
	//	++timeinfo2->tm_mday;

	//if (timeinfo2->tm_hour - timeinfo1->tm_hour == 0 && 
	//	timeinfo2->tm_min  - timeinfo1->tm_min  == 0 && 
	//	timeinfo2->tm_sec  - timeinfo2->tm_sec   < 0)
	//	++timeinfo2->tm_mday;

	if ( ( timeinfo2->tm_hour - timeinfo1->tm_hour < 0     ) ||
		 ( timeinfo2->tm_hour - timeinfo1->tm_hour == 0 && 
		   timeinfo2->tm_min  - timeinfo1->tm_min   < 0    ) ||
		 ( timeinfo2->tm_hour - timeinfo1->tm_hour == 0 && 
		   timeinfo2->tm_min  - timeinfo1->tm_min  == 0 && 
		   timeinfo2->tm_sec  - timeinfo2->tm_sec   < 0    )   )
	    ++timeinfo2->tm_mday;

	time_t newtime1 = mktime ( timeinfo1 );
	time_t newtime2 = mktime ( timeinfo2 );
	//tm* timeinfo2_1 = localtime(&newtime2);

	time_t updatetime = newtime2 - newtime1;

	
	//tm* updateTimeInfo = localtime(&updatetime);

	//int Pause = updateTimeInfo->tm_hour*60*60 + updateTimeInfo->tm_min*60 + updateTimeInfo->tm_sec;
 
	m_timer = boost::shared_ptr<CTimer>(new CTimer(m_io,CCRBTSevice::UpdateTone,/*Pause*/updatetime));

	delete(timeinfo1);
	delete(timeinfo2);

	m_pThread = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&boost::asio::io_service::run, &m_io)));
	//boost::thread t(boost::bind(&boost::asio::io_service::run, &m_io));
	m_pLog->LogString(LEVEL_INFO, L"Update timer has started, expired time: %s", _update_time.GetString());

}


void CCRBTSevice::UpdateTone()
{
	::CoInitialize(NULL);
	try
	{
		if (m_downloading.size())
		{
			// !!!! ERROR - we've got uncompleted loadings
		}
		m_downloading.clear();

		CConfigurationSettings settings;
		CString sCRBTAddressSubString,
			    sRealAddressSubString,
				sCatalogIDs;
		// Initiate database
		try
		{
			sCRBTAddressSubString = settings[ L"CRBTAddressSubString" ].ToString();
			sRealAddressSubString = settings[ L"RealAddressSubString" ].ToString();
		}
		catch(CConfigurationLoadException& _exception)
		{
			m_pLog->LogString(LEVEL_SEVERE,L"Cannot read CRBTAddressSubString or RealAddressSubString config keys: %s", _exception.ToString());
			throw;
		}

		try
		{
			sCatalogIDs = settings[L"CatalogIDs"].ToString();
		}
		catch(...)
		{
			sCatalogIDs = L"222 223 224 225 226 227";
		}

		//boost::shared_ptr<CADODatabase>database;
		//// Initiate database
		//try
		//{
		//	database = boost::shared_ptr<CADODatabase>(new CADODatabase());
		//	database->Open(settings[ L"ConnectionString" ].ToStr().c_str(), "", "", "", adUseClient);
		//}
		//catch(CConfigurationLoadException& _exception)
		//{
		//	m_pLog->LogString(LEVEL_SEVERE,L"Connect database string config param reading failed: %s", _exception.ToString());
		//	throw;
		//}
		//catch(_com_error& err) 
		//{
		//	m_pLog->LogString(LEVEL_SEVERE,L"Connect to database failed with error code: 0x%p : %s ", err.Error(), err.Description().GetBSTR());
		//	throw;
		//}
		//catch(...) 
		//{
		//	m_pLog->LogString(LEVEL_SEVERE,L"Unknown exception when try to connect to database");
		//	throw;
		//}
		CSmartADODataBase database;

		LPCWSTR p = sCatalogIDs, q;
		std::wstring sParamName, sParamVal;
		int k = 0;

		std::vector<CRBTInfo> RBTones;
		while (*p)
		{
			q = wcschr(p, L' ');
			if (!q)
			{
				sParamVal = Utils::trim(std::wstring(p),L';');
			}
			else
			{
				sParamVal = Utils::trim(std::wstring(p, q - p),L';');
			}
			if (!sParamVal.empty())
			{
				int stat = 0;
				CString CID= sParamVal.c_str();
				CString QueryToneResponse;
				boost::shared_ptr<CCRBTClient> soapclient( new CCRBTClient());;
				if (!m_soapclient->GetNewCRBT(QueryToneResponse, CID))
				{
					m_pLog->LogString(LEVEL_INFO,L"Cannot get data from SOAP server for ConfigID=\"%s\" ", CID);
					stat = 1;
					//throw CString(L"Cannot get data from SOAP server");
				}
				if (!stat)
				{
					////// !!!!!!!!
					//std::string full_path("D:\\Projects\\server2\\Debug\\post_1.xml");
					////std::string full_path("D:\\Projects\\1.xml");
					//std::ifstream is(full_path.c_str(), std::ios::in | std::ios::binary);
					//std::string resp;
					//char buf[512];
					//while (is.read(buf, sizeof(buf)).gcount() > 0)
					//	resp.append(buf, is.gcount());

					//QueryToneResponse = _bstr_t(resp.c_str()).GetBSTR();
					////////!!!!!!!!!!

					//CString ReplaceFTC = L"<fullTrackInfos xsi:type=\"soapenc:Array\" xsi:nil=\"true\"/>";
					//int pos = QueryToneResponse.Find(ReplaceFTC);
					//if (pos > 0)
					//{
					//	CString Right = QueryToneResponse.Right(pos + ReplaceFTC.GetLength() );
					//	CString Left  = QueryToneResponse.Left(pos);
					//	QueryToneResponse = Right+Left;
					//}
					QueryToneResponse.Replace(L"<fullTrackInfos xsi:type=\"soapenc:Array\" xsi:nil=\"true\"/>",L"");
					QueryToneResponse.Replace(L"<pictureInfos xsi:type=\"soapenc:Array\" xsi:nil=\"true\"/>",L"");
					QueryToneResponse.Replace(L"<toneBoxInfos xsi:type=\"soapenc:Array\" xsi:nil=\"true\"/>",L"");

					//std::wofstream f(L"respones.xml", std::ios::out | std::ios::binary);
					//f.write(QueryToneResponse.GetString(),QueryToneResponse.GetLength());
					//f.close();

					boost::system::error_code ec;
					boost::asio::io_service io;
					boost::asio::windows::random_access_handle file_(io);
					CParsedFileName pfn(CProcessHelper::GetCurrentModuleName());
					CString strRequestName = pfn.FullFolderName() + L"respones" + CID + L".xml";
					file_.assign(::CreateFile(strRequestName, GENERIC_WRITE, 0, 0,
						CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, 0), ec);
					if (file_.is_open())
					{
						file_.write_some_at(0, boost::asio::buffer(wtos(QueryToneResponse.GetString()).c_str(),QueryToneResponse.GetLength()));	
					}
					file_.close();
				}

				MSXML2::IXMLDOMNodePtr pNewDoc = NULL;
				if (!stat)
				{
					m_pLog->LogString(LEVEL_INFO,L"Load document");
					//::MessageBox(NULL, QueryToneResponse, L"SnmpExtAgent", MB_OK);

					if (FAILED(Load(_bstr_t(QueryToneResponse).GetBSTR(),L"",FALSE,pNewDoc)) || !pNewDoc)
						//if (FAILED(Load(L"D:\\Projects\\server2\\Debug\\XMLFile1.xml",L"",TRUE,pNewDoc)) || !pNewDoc)
					{
						//CString tempate(L"Cannot parse xml soap response: %s");
						//CString error; 
						//error.Format(tempate, ConfigID);
						////throw CString(L"Cannot parse xml soap response");
						//throw error;
						m_pLog->LogString(LEVEL_INFO,L"Cannot parse xml soap response for ConfigID=\"%s\" ", CID);
						stat = 1;

					}
				}
				//DecodeXML(QueryToneResponse)

				if (!stat)
				{
					m_pLog->LogString(LEVEL_INFO,L"Document ConfigID=\"%s\" load successful, parsing...", CID);
					MSXML2::IXMLDOMNodeListPtr	   NodeList;
					MSXML2::IXMLDOMNamedNodeMapPtr NodeMap;
					MSXML2::IXMLDOMElementPtr      NodeElem;
					MSXML2::IXMLDOMNodePtr	       Node;


					//Node = pNewDoc->selectSingleNode(L"/soapenv:Envelope/soapenv:Body"); example

					NodeList = pNewDoc->selectNodes(L"/soapenv:Envelope/soapenv:Body/multiRef");
					int i = 0;
					do
					{
						NodeElem = NodeList->Getitem(i);
						if (!NodeElem)
							break;
						NodeMap = NodeElem->Getattributes();

						Node = NodeMap->getNamedItem(L"xsi:type");
						CString id = _bstr_t(Node->text).GetBSTR();
						if (id.Find(L"ToneInfo")<0)
							continue;

						RBTones.push_back(CRBTInfo(NodeElem, sCRBTAddressSubString, sRealAddressSubString, CID));
					}
					while (NodeList->Getitem(++i));
				}

			}
			//break; // jut to testing
			if (!q)
			{
				break;
			}
			p = q + 1;
		}


		m_pLog->LogString(LEVEL_INFO,L"Total: %i records", RBTones.size());
		m_pLog->LogString(LEVEL_INFO,L"Cleanup proc call");
		CCleanupProc cln;
		if (!cln.Connect(database.get()))
		{
			throw CString(L"Cleanup proc - cannot connect to database");
		}
		if (cln.Call() == -1)
		{
			throw CString(L"Cleanup proc - execution failed");
		}
		m_pLog->LogString(LEVEL_INFO,L"Upload_data proc call");
		
		CUploadDataProc upl;
		if (!upl.Connect(database.get()))
		{
			// cannot connect to database
			throw CString(L"Upload_data proc - cannot connect to database");
		}
		m_pLog->LogString(LEVEL_INFO,L"Execute: \"set names cp1251\"");
//		upl.Execute(L"set names cp1251");
		for (unsigned int i=0;i<RBTones.size();++i)
		{
			database.Execute(_T("set names cp1251"));
			CRBTInfo * pMultyRef = (CRBTInfo *)(&RBTones[i]);
			if (!upl.SetAllParam(
				pMultyRef->GetToneID(),
				pMultyRef->GetToneCode(),
				pMultyRef->GetToneName(),
				pMultyRef->GetPrice(),
				pMultyRef->GetToneAddress(),
				pMultyRef->GetUpdateTime(),
				pMultyRef->GetCatalogID()
				/*,
				settings[L"localpath"].ToString()*/
				))
			{
				// cannot set upload_data proc params
				CString Err;
				Err.Format(L"Upload_data proc - error in incoming params %s", pMultyRef->Info());
				throw Err;
			}

			if (upl.Call() == -1)
			{
				throw CString(L"Upload_data proc - execution failed");
			}
		}
		
		//Get difference
		m_pLog->LogString(LEVEL_INFO,L"Get_diff proc call");
		CGetdiffProc gdf;
		if (!gdf.Connect(database.get()))
		{
			throw CString(L"Get_diff proc: cannot connect to database");
		}
		if (gdf.Call() == -1)
		{
			throw CString(L"Get_diff proc - execution failed");
		}

		GetDiff_RecordSet_type Diffrence;
		while (gdf.GetNextRecordSet(Diffrence))
		{
			CString StatusType(Diffrence.vType.bstrVal);
			if (StatusType == L"modified" || StatusType == L"added")
			{
				CString NewLocalPath = GetTempWavName(settings[L"TonesLocalPath"].ToString(), Diffrence.vTonePreListAddress.bstrVal);
				m_pLog->LogString(LEVEL_INFO,L"We've got new downloading - StatusType: %s, PreListAddress: %s, LocalPath: %s", StatusType.GetString(), CString(Diffrence.vTonePreListAddress.bstrVal).GetString(), NewLocalPath.GetString());
				m_downloading[m_transport->AddFileDownload
					(Diffrence.vTonePreListAddress.bstrVal, 
					NewLocalPath.GetString()
					)
				] = CRBTDownloadInfo(/*Utils::toStr(*/Diffrence.vIDTone.intVal/*).c_str()*/, NewLocalPath);

			}
			if (StatusType == L"deleted")
			{
				if (Diffrence.vLocalPath.vt!= VT_NULL) // if exist - delete it
				{
					CString DeletedFileName(Diffrence.vLocalPath.bstrVal);
					m_pLog->LogString(LEVEL_INFO, L"Deleting old wav file, LocalAddress: %s", DeletedFileName);
					::DeleteFile(DeletedFileName.GetString());
				}
			}
		}

		//Finishing
		m_pLog->LogString(LEVEL_INFO,L"Copy_data proc call");
		CCopydataProc cpy;
		if (!cpy.Connect(database.get()))
		{
			throw CString(L"Copy_data proc: cannot connect to database");		
		}
		if (cpy.Call() == -1)
		{
			throw CString(L"Copy_data proc - execution failed");
		}



	}
	catch (_com_error& err)
	{
		m_pLog->LogString(LEVEL_SEVERE,L"Update RBT failed with error code: 0x%p : %s ", err.Error(), err.Description().GetBSTR());
	}
	catch(CConfigurationLoadException& _exception)
	{
		m_pLog->LogString(LEVEL_SEVERE,L"config reading failed: %s", _exception.ToString());
	}
	catch (CString& strError)
	{
		m_pLog->LogString(LEVEL_SEVERE,L"Update RBT failed with error : %s ", strError);
	}
	 catch(boost::system::system_error& e)
	 {
		 m_pLog->LogString(LEVEL_SEVERE, L"asio exception: %s", CString(e.what()).GetString());
	 }
	catch (...)
	{
		m_pLog->LogString(LEVEL_SEVERE, L"Unknown exception, when initialize CRBT try to update RBT");
	}
	::CoUninitialize();
}


void CCRBTSevice::OnDownloadFailure(int _jobId)
{
	try
	{
		TransferFiles::const_iterator cit = m_downloading.find(_jobId);
		if (cit!=m_downloading.end())
		{
			m_pLog->LogString(LEVEL_INFO, L"download rbt with toneID: %i has failed", cit->second.GetToneID());
		}
		else
		{
			throw(CString(L"we've got response from unknown downloading"));
		}
	}
	catch (_com_error& err)
	{
		m_pLog->LogString(LEVEL_SEVERE,L"Update RBT failed with error code: 0x%p : %s ", err.Error(), err.Description().GetBSTR());
	}
	//catch(CConfigurationLoadException& _exception)
	//{
	//	m_pLog->LogString(LEVEL_FINE,L"config reading failed: %s", _exception.ToString());
	//}
	catch (CString& strError)
	{
		m_pLog->LogString(LEVEL_SEVERE,L"Update RBT failed with error : %s ", strError);
	}
	catch (...)
	{
		m_pLog->LogString(LEVEL_SEVERE, L"Unknown exception, when initialize CRBT try to update RBT");
	}}

void CCRBTSevice::OnDownloadSuccess(int _jobId)
{
	//CConfigurationSettings settings;
	//boost::shared_ptr<CADODatabase>database;
	//// Initiate database
	//try
	//{
	//	database = boost::shared_ptr<CADODatabase>(new CADODatabase());
	//	database->Open(settings[ L"ConnectionString" ].ToStr().c_str(), "", "", "", adUseClient);
	//}
	//catch(CConfigurationLoadException& _exception)
	//{
	//	m_pLog->LogString(LEVEL_SEVERE,L"Connect database string config param reading failed: %s", _exception.ToString());
	//	throw;
	//}
	//catch(_com_error& err) 
	//{
	//	m_pLog->LogString(LEVEL_SEVERE,L"Connect to database failed with error code: 0x%p : %s ", err.Error(), err.Description().GetBSTR());
	//	throw;
	//}
	//catch(...) 
	//{
	//	m_pLog->LogString(LEVEL_SEVERE,L"Unknown exception when try to connect to database");
	//	throw;
	//}
	CSmartADODataBase database;
	// update database
	CString strFilename;
	try
	{
		TransferFiles::const_iterator cit = m_downloading.find(_jobId);
		if (cit!=m_downloading.end())
		{
			strFilename = cit->second.GetLocalPath();
			m_pLog->LogString(LEVEL_INFO, L"File download completed, LocalAddress: %s  - , update database", strFilename);
			// Convert
			ConvertWav(strFilename);

			CUpdateToneProc upd;
			m_pLog->LogString(LEVEL_INFO,L"Update_tone proc call");
			if (!upd.Connect(database.get()))
			{
				// cannot connect to database
				throw CString(L"Update_tone proc - cannot connect to database");
			}
			
			if (!upd.SetAllParam(cit->second.GetToneID(), strFilename/*cit->second.GetLocalPath()*/))
			{
				// cannot set upload_data proc params
				CString Err;
				Err.Format(L"Update_tone proc - error in incoming params - toneID: %i, LocalPath: %s", cit->second.GetToneID(), cit->second.GetLocalPath());
				throw Err;
			}

			if (upd.Call() == -1)
			{
				throw CString(L"Update_tone proc - execution failed");
			}
			m_downloading.erase(cit);
		}
		else
		{
			throw(CString(L"we've got response from unknown downloading"));
		}
	}
	catch (_com_error& err)
	{
		m_pLog->LogString(LEVEL_SEVERE,L"Update RBT failed with error code: 0x%p : %s ", err.Error(), err.Description().GetBSTR());
		return;
	}
	//catch(CConfigurationLoadException& _exception)
	//{
	//	m_pLog->LogString(LEVEL_FINE,L"config reading failed: %s", _exception.ToString());
	//}
	catch (CString& strError)
	{
		m_pLog->LogString(LEVEL_SEVERE,L"Update RBT failed with error : %s ", strError);
		return;
	}
	catch (...)
	{
		m_pLog->LogString(LEVEL_SEVERE, L"Unknown exception, when initialize CRBT try to update RBT");
		return;
	}
}

CString CCRBTSevice::GetTempWavName(const CString& _temp_folder, const CString& _fileurl)
{
	CString file_name = _fileurl.Right(_fileurl.GetLength() - _fileurl.ReverseFind('/') -1);
	if (!file_name.GetLength())
	{
		file_name = L"rbt_";
	}
	else
	{
		file_name = file_name.Left(file_name.Find('.'));
		file_name+= L"_";
	}

	TCHAR szTempName[MAX_PATH];
	::GetTempFileName(_temp_folder.GetString(), file_name.GetString(), rand(), szTempName);
	//UINT stat = ::GetTempFileName(L"c:\\", L"NEW", 0, szTempName);
	file_name = szTempName;
	file_name = file_name.Left(file_name.Find('.'));
	file_name+=L".wav";
	return file_name;
}
void CCRBTSevice::ConvertWav(CString& _name)
{
	CString saveFileName(_name);
	try
	{
		int iConvertFormat = RAW_FORMAT;
		if (!m_sConvertFormat.CompareNoCase(L"RAW"))
		{
			// already RAW
		}
		else if (!m_sConvertFormat.CompareNoCase(L"PCM"))
			iConvertFormat = WAVE_FORMAT_PCM;
		else if (!m_sConvertFormat.CompareNoCase(L"ALAW"))
			iConvertFormat = WAVE_FORMAT_ALAW;
		else if (!m_sConvertFormat.CompareNoCase(L"3GP"))
			iConvertFormat = F3GP_FORMAT;
		else
			iConvertFormat = WAVE_FORMAT_PCM;

		m_pLog->LogString(LEVEL_INFO, L"Convert wav file into %s format with BitsPerSample: %i, SamplesPerSecond: %i",m_sConvertFormat, m_nDefaultBitsPerSample, m_nDefaultSamplesPerSecond);
		switch (iConvertFormat)
		{
		case WAVE_FORMAT_ALAW:
		case WAVE_FORMAT_PCM:
			{
				CConvertAction::Convert(_name, iConvertFormat, m_nDefaultBitsPerSample, m_nDefaultSamplesPerSecond);
				break;
			}
		case RAW_FORMAT:
			{
				// We have to do 3 actions:
				//   1. Convert *.wav into PCM *.wav
				//   2. Change the extention to *.raw
				//   3. Convert into RAW 

				//1.
				CConvertAction::Convert(_name, WAVE_FORMAT_PCM, m_nDefaultBitsPerSample, m_nDefaultSamplesPerSecond);
				//2. 
				CString strRequestName(_name);
				strRequestName.Replace(L".wav",L".raw");
				//3.
				CWave raw_converter;
				raw_converter.Load(_name);
				boost::system::error_code ec;
				boost::asio::io_service io;
				boost::asio::windows::random_access_handle file_(io);
				file_.assign(::CreateFile(strRequestName, GENERIC_WRITE, 0, 0,
					CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, 0), ec);
				if (file_.is_open())
				{
					file_.write_some_at(0, boost::asio::buffer(raw_converter.GetBuffer(),raw_converter.GetBufferLength()));	
				}
				file_.close();
				_name = strRequestName;

				break;
			}
		case F3GP_FORMAT:
			{
				if (m_3gpConverterFilePath.IsEmpty())
				{
					m_pLog->LogString(LEVEL_SEVERE,L"Could not find 3gp converter, converting failed");
					throw 1;
				}
				// 1. Rename file
				CString strRequestName(_name);
				strRequestName.Replace(L".wav",L".3gp");
				// 2. Convert *.wav into *.3gp format
				CString sCommandLine = m_3gpConverterFilePath + L" " + _name + L" " + strRequestName;
				STARTUPINFO si;
				PROCESS_INFORMATION pi;
				ZeroMemory( &si, sizeof(si) );
				si.cb = sizeof(si);
				ZeroMemory( &pi, sizeof(pi) );

				//bool bSuccess = ::CreateProcess(NULL, L"d:\\Projects\\crbt\\Obj\\3gpConverter\\convert.bat C:\\is3\\tones\\Pu.wav C:\\is3\\tones\\Pu.3gp"/*(WCHAR*)sCommandLine.GetString()*/, 
				//	NULL,NULL,FALSE,0,NULL,NULL,&si,&pi);
				CExecutor exec(sCommandLine);
				//if (!bSuccess)
				//{
				//	m_pLog->LogString(LEVEL_SEVERE,L"Created process \"%s  %s\" failed", m_3gpConverterFilePath, sCommandLine);
				//	throw 1;
				//}
				//WaitForSingleObject( pi.hProcess, INFINITE );
				//CloseHandle( pi.hProcess );
				//CloseHandle( pi.hThread );
				// 3. Check, that file physically exist. throw exception otherwise
				std::ifstream is(strRequestName, std::ios::in | std::ios::binary);
				if (!is.is_open())
				{
					m_pLog->LogString(LEVEL_SEVERE,L"Could not find 3gp converted file : %s, converting failed ", strRequestName);
					throw 1;
				}
				_name = strRequestName;

				break;
			}

		};
	}
	catch(...)
	{
		m_pLog->LogString(LEVEL_SEVERE, _T("Auto conversion of %s failed"), _name.GetString());
		_name = saveFileName; // return name
	}
}

/******************************* eof *************************************/
/************************************************************************/
/* Name     : crbt\uploaddata.h                                         */
/* Author   : Andrey Alekseev                                           */
/* Project  : CRBT                                                      */
/* Company  : Forte-CT                                                  */
/* Date     : 04 Mar 2011                                               */
/************************************************************************/
#pragma once
#include "..\ado\ADODatabase.h"
#include "logger.h"
#include "ConfigurationSettings.h"

class CUploadDataProc: public CADOStoredProc
{
public:
	CUploadDataProc();
	virtual ~CUploadDataProc();

	bool AdAllParam();
	bool Connect(CADODatabase *pDB = NULL);
	bool SetAllParam(
		const int& _tone_id,
		const int& _tone_code,
		const CString& _tone_name,
		const CString& _price,
		const CString& _address,
		const COleDateTime& _update_time,
		const int& _catalogid/*,
		const CString& _local_path*/
		);

	int Call();

private:
	singleton_auto_pointer<CSystemLog> m_pLog;

};


/******************************* eof *************************************/
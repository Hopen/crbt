/************************************************************************/
/* Name     : crbt\timer.h                                              */
/* Author   : Andrey Alekseev                                           */
/* Project  : CRBT                                                      */
/* Company  : Forte-CT                                                  */
/* Date     : 11 Mar 2011                                               */
/************************************************************************/
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

const boost::posix_time::hours tUpdate(24);

typedef void (*SimpleCallbackFunc)(/*void *param*/);

class CTimer
{
public:
	CTimer(boost::asio::io_service& io,
		SimpleCallbackFunc _callback, 
		const int& TimeToFirstExpire/*,
		void* param*/
		);
	~CTimer();
	void Cancel();
	bool IsCanceled(){return m_bCanceled;}

private:
	boost::asio::deadline_timer m_timer1;
	SimpleCallbackFunc          m_CallBack;
	//void * m_param;

	// events
	void Timer1Expired();

	bool m_bCanceled;

};

/******************************* eof *************************************/
/************************************************************************/
/* Name     : crbt\cleanup.cpp                                          */
/* Author   : Andrey Alekseev                                           */
/* Project  : CRBT                                                      */
/* Company  : Forte-CT                                                  */
/* Date     : 04 Mar 2011                                               */
/************************************************************************/
#include "stdafx.h"
#include "cleanup.h"

CCleanupProc::CCleanupProc()
{

}

CCleanupProc::~CCleanupProc()
{

}

int CCleanupProc::Call()
{
	if (Execute()) {
		//return GetRcParam();
		return 0;
	}
	return -1;
}

bool CCleanupProc::AdAllParam()
{
	bool bRc = true;
	//bRc &= AdParam( _T("RETURN_VALUE"),    adInteger, adParamReturnValue, 4);
	return bRc;

}



bool CCleanupProc::Connect(CADODatabase *pDB /* = NULL */)
{
	try
	{
		CConfigurationSettings settings;
		//if (pDB) 
		//{
		//	// Use existing connection
		//	if (Initialize( L"cleanup", pDB))
		//		return AdAllParam();
		//}
		//else 
		//{
		//	// Open new connection
		//	if (Initialize( L"cleanup", settings[ L"ConnectionString" ].ToStr().c_str(), "", ""))
		//		return AdAllParam();
		//}
		return __super::Connect(L"cleanup", settings[ L"ConnectionString" ].ToStr().c_str(), pDB);
	}
	catch(CConfigurationLoadException& _exception)
	{
		m_pLog->LogString(LEVEL_FINE,L"Connect database string config param reading failed: %s", _exception.ToString());
	}
	catch(_com_error& err) 
	{
		m_pLog->LogString(LEVEL_INFO,L"Connect to database failed with error code: 0x%p : %s ", err.Error(), err.Description().GetBSTR());
	}
	catch(...) 
	{
		m_pLog->LogString(LEVEL_INFO,L"Unknown exception when try to connect to database");
	}
	return false;
}




/******************************* eof *************************************/
/************************************************************************/
/* Name     : crbt\updatetone.cpp                                       */
/* Author   : Andrey Alekseev                                           */
/* Project  : CRBT                                                      */
/* Company  : Forte-CT                                                  */
/* Date     : 04 Mar 2011                                               */
/************************************************************************/
#include "stdafx.h"
#include "updatetone.h"

CUpdateToneProc::CUpdateToneProc()
{

}

CUpdateToneProc::~CUpdateToneProc()
{

}

int CUpdateToneProc::Call()
{
	if (Execute()) {
		//return GetRcParam();
		return 0;
	}
	return -1;
}

bool CUpdateToneProc::AdAllParam()
{
	bool bRc = true;
	//bRc &= AdParam( _T("RETURN_VALUE"),    adInteger, adParamReturnValue, 4);
	bRc &= AdParam( _T("ToneId_in"              ),  adInteger    , adParamInput, 11);
	bRc &= AdParam( _T("localPath_in"           ),  adVarChar    , adParamInput, 255);
	return bRc;

}

bool CUpdateToneProc::SetAllParam(
								  const int& _tone_id,
								  const CString& _local_path
								  )
{
	bool bRc = true;
	bRc &= SetParam( _T("ToneId_in"                 ), _tone_id  ); 
	bRc &= SetParam( _T("localPath_in"              ), _local_path.GetString()); 

	return bRc;
}

bool CUpdateToneProc::Connect(CADODatabase *pDB /* = NULL */)
{
	try
	{
		CConfigurationSettings settings;
		return __super::Connect(L"update_tone", settings[ L"ConnectionString" ].ToStr().c_str(), pDB);
	}
	catch(CConfigurationLoadException& _exception)
	{
		m_pLog->LogString(LEVEL_FINE,L"Connect database string config param reading failed: %s", _exception.ToString());
	}
	catch(_com_error& err) 
	{
		m_pLog->LogString(LEVEL_INFO,L"Connect to database failed with error code: 0x%p : %s ", err.Error(), err.Description().GetBSTR());
	}
	catch(...) 
	{
		m_pLog->LogString(LEVEL_INFO,L"Unknown exception when try to connect to database");
	}
	return false;
}


/******************************* eof *************************************/
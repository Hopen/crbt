/************************************************************************/
/* Name     : crbt\soapclient.cpp                                       */
/* Author   : Andrey Alekseev                                           */
/* Project  : CRBT                                                      */
/* Company  : Forte-CT                                                  */
/* Date     : 21 Feb 2011                                               */
/************************************************************************/
#include "stdafx.h"
#include "resource.h"
#include "soapclient.h"
#include "ConfigurationSettings.h"

WCHAR EndRecordNum       [] = L"<endRecordNum xsi:type=\"xsd:string\">%s</endRecordNum>";
WCHAR StartRecordNum     [] = L"<startRecordNum xsi:type=\"xsd:string\">%s</startRecordNum>";
WCHAR EndRecordNum_nil   [] = L"<endRecordNum xsi:type=\"xsd:string\" xsi:nil=\"true\" />";
WCHAR StartRecordNum_nil [] = L"<startRecordNum xsi:type=\"xsd:string\" xsi:nil=\"true\" />";

CSoapClientBase::CSoapClientBase()
{
}

void CSoapClientBase::SetRequestHeader(LPCWSTR _lpszHeader, LPCWSTR _lpszText)
{
	HRESULT hRes = pHTTPRequest->SetRequestHeader(_bstr_t(_lpszHeader), _bstr_t(_lpszText));
	if(!SUCCEEDED(hRes))
		throw _com_error(hRes);
}

HRESULT CSoapClientBase::PrepareHTTPRequest(const _bstr_t& _bstrUrl)
{
	HRESULT hRes = S_OK;
	if (!pHTTPRequest)
	   hRes = pHTTPRequest.CoCreateInstance(CLSID_WinHttpRequest);

	if(!SUCCEEDED(hRes))
		return hRes;

	hRes = pHTTPRequest->Open(_bstr_t(L"POST"), /*_bstr_t(m_strRPPUrl)*/ _bstrUrl, variant_t(0));//, variant_t(0), variant_t(0));

	if(!SUCCEEDED(hRes))
	{
		//m_pLog->LogStringModule(LEVEL_INFO, _T("OSA/Parlay client"), _T("Failed to connect to %s"), m_strRPPUrl);
		return hRes;
	}

	SetRequestHeader(L"Content-type",	L"text/xml; charset=utf-8");
	SetRequestHeader(L"Accept",			L"application/soap+xml, application/dime, multipart/related, text/*");
	//SetRequestHeader(L"User-Agent",		L"Axis/1.3");
	//SetRequestHeader(L"Host",			L"10.97.34.34:8080");
	SetRequestHeader(L"Cache-Control",	L"no-cache");
	SetRequestHeader(L"Pragma",			L"no-cache");

	return S_OK;
}

HRESULT CSoapClientBase::SendSoapMessage(const _bstr_t& _bstrSoapAction, const _bstr_t& _bstrSoapText, LONG _timeout, LONG* _lResult, _bstr_t& _bstrText)
{
	SystemLog pLog;
	CString strResult;
	CString strSoap = _bstrSoapText;
	CString strLength;

	strLength.Format(_T("%i"), strSoap.GetLength());

	SetRequestHeader(L"SOAPAction",		_bstrSoapAction);
	SetRequestHeader(L"Content-Length",	_bstr_t(strLength));

	pHTTPRequest->Send(variant_t(strSoap.GetString()));

	VARIANT_BOOL	vbWaitResult = VARIANT_FALSE;
	vbWaitResult = pHTTPRequest->WaitForResponse(variant_t(_timeout));
	
	if(vbWaitResult == FALSE)
	{
		*_lResult = -1;
		_bstrText = L"";
		return S_OK;
	}

	long	lStatus   = -1;
	_bstr_t bsStatusText = L"";
	_bstr_t bsResponseText = L"";

	lStatus = pHTTPRequest->Status;
	bsStatusText = pHTTPRequest->StatusText;
	bsResponseText = pHTTPRequest->ResponseText;

	pLog->LogStringModule(LEVEL_INFO, _T("SOAP"), _T("Status: %i, StatusText: %s, Response: %s"), lStatus, (LPCTSTR)bsStatusText, (LPCTSTR)bsResponseText);
	::Sleep(1000);

	*_lResult = lStatus;
	//*_bstrText = bsResponseText;
	_bstrText = bsResponseText;

	return S_OK;
}

CCRBTClient::CCRBTClient()
{
	m_pClientBase = CSoapClientBase::GetInstance();
	m_startRecordNum = L"0";
	m_endRecordNum = L"0";
	m_dwTimeout    = 5;
	m_EnableRecordNum = false;

	try
	{
		CConfigurationSettings settings;
		m_dwTimeout       = settings[ L"ConnectTimeOut"  ].ToInt   ();
		m_SoapAction      = settings[ L"SoapAction"      ].ToString();
		m_strRPPUrl       = settings[ L"CRBTUrl"         ].ToString();
		m_portalAccount   = settings[ L"PortalAccount"   ].ToString();
		m_portalPwd       = settings[ L"PortalPwd"       ].ToString();
		m_startRecordNum  = settings[ L"StartRecordNum"  ].ToString();
		m_endRecordNum    = settings[ L"EndRecordNum"    ].ToString();
		CString sEnableRecordNum;
		sEnableRecordNum  = settings[ L"EnableRecordNum" ].ToString();
		m_EnableRecordNum = !sEnableRecordNum.CompareNoCase(L"true");
	}	
	catch(CConfigurationLoadException& _exception)
	{
		m_pLog->LogString(LEVEL_FINE,L"Config params reading failed: %s", _exception.ToString());
	}
	catch(...)
	{
		m_pLog->LogString(LEVEL_FINE, L"Unknown exception, when initialize CRBT soap client");
	}
}
bool CCRBTClient::GetNewCRBT(CString& _response, const CString& sCatalogID)
{
	m_pLog->LogString(LEVEL_INFO, _T("Prepare HTTP request"));
	if (FAILED(m_pClientBase->PrepareHTTPRequest(_bstr_t(m_strRPPUrl))))
	{
		m_pLog->LogStringModule(LEVEL_INFO, _T("CRBT soap client"), _T("Failed to connect to %s"), m_strRPPUrl);
		return false;
	}
	CString strSoap;
	CString strStartRecordNum;
	CString strEndRecordNum;
	strStartRecordNum.Format(StartRecordNum,m_startRecordNum);
	strEndRecordNum  .Format(EndRecordNum  ,m_endRecordNum  );

	strSoap.Format(/*IDS_SOAPREQUEST*/IDS_SAOPCATREQUEST, m_portalAccount, m_portalPwd, m_EnableRecordNum?strEndRecordNum:EndRecordNum_nil, m_EnableRecordNum?strStartRecordNum:StartRecordNum_nil, sCatalogID );

	long lResult;
	_bstr_t bstrResult=L"";
	m_pLog->LogString(LEVEL_INFO, _T("Send SOAP message"));
	//return false;
	if FAILED(m_pClientBase->SendSoapMessage(_bstr_t(m_SoapAction), _bstr_t(strSoap), m_dwTimeout, &lResult, bstrResult))
	{
		m_pLog->LogStringModule(LEVEL_INFO, _T("CRBT soap client"), _T("Failed to send request %s"), strSoap);
		return false;
	}

	m_pLog->LogString(LEVEL_INFO, _T("Response length: %i"), bstrResult.length());
	_response = bstrResult.GetBSTR();
	return true;
}
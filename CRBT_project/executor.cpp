#include "stdafx.h"
#include "boost/scoped_array.hpp"
#include "singleton.h"
#include "logger.h"
#include "SystemException.h"
#include "executor.h"


CExecutor::CExecutor(LPCWSTR _lpszCommandLine)
{
	singleton_auto_pointer<CSystemLog> pLog;
	try
	{
		PROCESS_INFORMATION piProcInfo = {0}; 
		STARTUPINFOW siStartInfo = {0};

		siStartInfo.cb = sizeof(STARTUPINFO); 
		//siStartInfo.hStdError = hChildStdoutWr;
		//siStartInfo.hStdOutput = hChildStdoutWr;
		//siStartInfo.hStdInput = hChildStdinRd;
		siStartInfo.wShowWindow = SW_HIDE;
		siStartInfo.dwFlags = STARTF_USESHOWWINDOW;

		// Create the child process. 

		WCHAR wszCmdLine[MAX_PATH * 2] = {0};
		wcsncpy(wszCmdLine, _lpszCommandLine, MAX_PATH * 2 - 1);

		if (CreateProcessW(
			NULL, 
			wszCmdLine,   // command line 
			NULL,    // process security attributes 
			NULL,    // primary thread security attributes 
			TRUE,    // handles are inherited 
			CREATE_NO_WINDOW, // creation flags 
			NULL,    // use parent's environment 
			NULL,    // use parent's current directory 
			&siStartInfo,  // STARTUPINFO pointer 
			&piProcInfo) == 0) // receives PROCESS_INFORMATION 
		{
			THROW_SYSTEM_EXCEPTION();
		}

		//Create(_lpszCommandLine);
		pLog->LogString(LEVEL_INFO, _T("Executing: %s"), _lpszCommandLine);
		::WaitForSingleObject(piProcInfo.hProcess, 60*60*1000);
		::CloseHandle(piProcInfo.hProcess);
		pLog->LogString(LEVEL_INFO, _T("Process finished: %s"), _lpszCommandLine);
		//WaitForProcess();
	}
	catch (CExceptionBase& _ex)
	{
		pLog->LogString(LEVEL_WARNING, _T("Failed to execute: %s"), _lpszCommandLine);
		pLog->LogString(LEVEL_WARNING, _T("%s"), _ex.ToString());
	}
}

void CExecutor::OnRead(LPCSTR pszData, DWORD dwSize)
{
	::CoInitialize(NULL);
	boost::scoped_array<char> pArray(new char[dwSize + 1]);
	ZeroMemory(pArray.get(), dwSize + 1);
	memcpy(pArray.get(), pszData, dwSize);
	singleton_auto_pointer<CSystemLog> pLog;
	pLog->LogString(LEVEL_INFO, _T("Execution result: %s"), (LPCTSTR)_bstr_t(pArray.get()));
	::CoUninitialize();
}
/************************************************************************/
/* Name     : crbt\utils.h                                              */
/* Author   : Andrey Alekseev                                           */
/* Project  : CRBT                                                      */
/* Company  : Forte-CT                                                  */
/* Date     : 04 Mar 2011                                               */
/************************************************************************/

#pragma once
#include <string>
#include <sstream> 

namespace Utils
{

	////////////////////////////////////////////////////////////////////////////////
	//service templates for trim operations

	template<class TStr,class TTrimComp>
	TStr trimr(const TStr& str,TTrimComp is_trim_char)
	{
		TStr::const_iterator i;

		for(i=str.end()-1;
			i>=str.begin() && is_trim_char(*i);
			--i);

		return TStr(str.begin(),++i);
	}

	template<class TStr,class TTrimComp>
	TStr triml(const TStr& str,TTrimComp is_trim_char)
	{
		TStr::const_iterator i;

		for(i=str.begin();i!=str.end() && is_trim_char(*i);++i);

		return TStr(i,str.end());
	}

	template<class TStr,class TTrimComp>
	TStr trim(const TStr& str,TTrimComp is_trim_char)
	{
		return triml(trimr(str,is_trim_char),is_trim_char);
	}


	inline std::wstring trimr(const std::wstring& str, const WCHAR& trim_char)
	{return trimr(str,std::bind1st(std::equal_to<WCHAR>(),trim_char));}

	inline std::wstring triml(const std::wstring& str, const WCHAR& trim_char)
	{return triml(str,std::bind1st(std::equal_to<WCHAR>(),trim_char));}

	inline std::wstring trim(const std::wstring& str, const WCHAR& trim_char)
	{return trim(str,std::bind1st(std::equal_to<WCHAR>(),trim_char));}

	template<class T>
	std::wstring toStr(T aa)
	{
		std::wostringstream out;
		out << aa;
		return out.str();
	}

	template<class T>
	T toNum(std::wstring _in)
	{
		T num(0);
		std::wistringstream inn(_in);
		inn >> num;
		return num;
	}

	template<class T>
	T toNumX(std::wstring _in)
	{
		T num;
		if (_in.empty())
			num = 0;
		else
			std::wstringstream(_in) >> std::hex >> num;
		return num;
	}

	template <class T>
	T wtoi(std::wstring in)
	{
		T ret = 0;
		bool bTwos = false;
		if (in[0]=='-')
			bTwos = true;
		for (unsigned int i=bTwos?1:0;i<in.size();++i)
		{
			ret *= 10;
			ret += (in[i] - L'0');
		}
		if (bTwos)
			ret = 0 - ret;
		return ret;
	}
}

/******************************* eof *************************************/
// test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


int _tmain(int argc, _TCHAR* argv[])
{
	::CoInitialize(NULL);

	HRESULT hr = S_OK;
	IDispatch* unk = NULL;
	hr = ::CoCreateInstance(CLSID_CRBT,0,CLSCTX_ALL,IID_IDispatch,(LPVOID*)&unk);
	if (FAILED(hr))
		return -1;
	ICRBT *point = NULL;
	hr = unk->QueryInterface(IID_ICRBT,(void**)&point);
	if (FAILED(hr))
		return -1;

	point->Update();

	point->Release();
	unk->Release();

	::CoUninitialize();

	return 0;
}


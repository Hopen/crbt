#pragma once
#include "monitorthread.h"
#include "ExceptionBase.h"
#include <MMReg.h>

class CContentManager;

class CConversionException : public CExceptionBase
{
public:
	CConversionException(LPCTSTR _lpszExceptionText, LPCTSTR _lpszFileName, int _lineNumber) : CExceptionBase(_lpszExceptionText, _T("Sound conversion failure"), _lpszFileName, _lineNumber)
	{
	}

	CConversionException(const CConversionException& _exception) : CExceptionBase(_exception)
	{
	}

	virtual ~CConversionException(void)
	{
	}
};

class CConvertAction : public IMonitorAction
{
	//CString				m_strFileName;
	//int					m_nFormatId;
	//int					m_nBitsPerSample;
	//int					m_nSamplesPerSecond;

	//__int64 FormatTimeStamp(const SYSTEMTIME& time);
	//CString FormatTime(const SYSTEMTIME& time);

	//void SendConversionFailedEvent(int _errorCode, LPCTSTR _lpszDescription);

public:
	//CConvertAction(LPCTSTR _lpszFileName, int _nFormatId, int _nBitsPerSample, int _nSamplesPerSecond);
	//~CConvertAction();

	//virtual	bool ExecuteAction();

	static bool Convert(LPCTSTR _lpszFileName, int _nFormatId, int _nBitsPerSample, int _nSamplesPerSecond);
	//static bool IsWaveFile(LPCTSTR _lpszFileName);
	//static void AutoConvertFile(LPCTSTR _lpszFileName);
	//static void ReadHeader(LPCTSTR _lpszFileName, WAVEFORMATEX& _format);
};

#include "StdAfx.h"
//#include ".\convertaction.h"
#include "wavutil.h"
#include "boost/scoped_array.hpp"
//#include "boost/shared_array.hpp"
//#include "logger.h"
//#include "ContentManager.h"
//#include "EvtModel.h"
#include "SystemException.h"
//#include "SystemUtils.h"
//#include "ReplicationManager2.h"
//#include "RouterManager.h"
//#include <MMReg.h>
//#include "BinaryFile.h"
#include "ConvertAction.h"

//CConvertAction::CConvertAction(LPCTSTR _lpszFileName, int _nFormatId, int _nBitsPerSample, int _nSamplesPerSecond) : 
//IMonitorAction(_lpszFileName),
//m_strFileName(_lpszFileName), 
//m_nFormatId(_nFormatId), 
//m_nBitsPerSample(_nBitsPerSample),
//m_nSamplesPerSecond(_nSamplesPerSecond)
//{
////	/* FIXME
//	singleton_auto_pointer<CReplicationManager> pManager;
//	pManager->AddWatchFile(_lpszFileName);
////	*/
//}
//
//CConvertAction::~CConvertAction()
//{
////	/* FIXME
//	singleton_auto_pointer<CReplicationManager> pManager;
//	pManager->RemoveWatchFile(m_strFileName);
////	*/
//}

//bool CConvertAction::ExecuteAction()
//{
//	try
//	{
//		Convert(m_strFileName, m_nFormatId, m_nBitsPerSample, m_nSamplesPerSecond);
//		return true;
//	}
//	catch(CExceptionBase& exception)
//	{
//		SendConversionFailedEvent(0xFFFFFFFF, exception.Description);
//		return true;
//	}
//}
//
//void CConvertAction::SendConversionFailedEvent(int _errorCode, LPCTSTR _lpszDescription)
//{
//	__int64 nAlarmId = (__int64(rand()) << 32) | rand();
//
//	SYSTEMTIME eventTime, importanceTime;
//	::GetSystemTime(&eventTime);
//	ZeroMemory(&importanceTime, sizeof(importanceTime));
//	importanceTime.wMinute = 2;
//
//	COleDateTime odtNow = COleDateTime::GetCurrentTime();
//	SYSTEMTIME formatTime;
//	odtNow.GetAsSystemTime(formatTime);
//
//	CMessage message;
//	message.Name = L"SYSTEM_ALARM";
//	message[L"AlarmID"]				= nAlarmId;
//	message[L"Severity"]				= "Major";
//
//	message[L"ActivationTime"]		= FormatTime(formatTime);
//	message[L"ActivationTimestamp"]	= FormatTimeStamp(eventTime);
//
//	message[L"ImportanceTime"]		= FormatTime(importanceTime);
//	message[L"ImportanceTimestamp"]	= importanceTime.wMinute * 60 * 1000; //FormatTimeStamp(importanceTime);
//
//	message[L"Machine"]				= CProcessHelper::GetComputerName();
//	message[L"Module"]				= "Content Replicator";
//	message[L"SubSystem"]			= "Converter";
//	message[L"ErrorCode"]			= _errorCode;
//	message[L"ErrorName"]			= "System error";
//	message[L"ErrorDescription"]	= _lpszDescription;
//
//	singleton_auto_pointer<CRouterManager> manager;
//	manager->SendToAny(message);
//}
//
//__int64 CConvertAction::FormatTimeStamp(const SYSTEMTIME& systime)
//{
//	FILETIME fileTime;
//	::SystemTimeToFileTime(&systime, &fileTime);
//	return *((__int64 *)&fileTime);
//}
//
//CString CConvertAction::FormatTime(const SYSTEMTIME& systime)
//{
//	CString strResult;
//	strResult.Format(_T("%02i-%02i-%04i %02i:%02i:%02i"), 
//		systime.wDay, systime.wMonth, systime.wYear,
//		systime.wHour, systime.wMinute, systime.wSecond);
//
//	return strResult;
//}

bool CConvertAction::Convert(LPCTSTR _lpszFileName, int _nFormatId, int _nBitsPerSample, int _nSamplesPerSecond)
{
	singleton_auto_pointer<CSystemLog> pLog;
	pLog->LogStringModule(LEVEL_INFO, _T("WaveConverter"), _T("Converstion of file %s started..."), _lpszFileName);
	WAVEFORMATEX fmt;

	fmt.nChannels			= 1;
	fmt.nSamplesPerSec		= _nSamplesPerSecond;
	fmt.wBitsPerSample		= _nBitsPerSample;
	fmt.wFormatTag			= _nFormatId;
	fmt.nBlockAlign			= (_nBitsPerSample * fmt.nChannels) / 8;
	fmt.cbSize				= 0;
	fmt.nAvgBytesPerSec		= _nSamplesPerSecond * fmt.nBlockAlign;

	if(::GetFileAttributes(_lpszFileName) & FILE_ATTRIBUTE_READONLY)
		pLog->LogStringModule(LEVEL_SEVERE, _T("WaveConverter"), _T("Read only file %s found"), _lpszFileName);
	ATL::CHandle hFile;
	hFile.Attach(::CreateFile(_lpszFileName, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL));
	if(hFile == INVALID_HANDLE_VALUE)
	{
		pLog->LogStringModule(LEVEL_INFO, _T("WaveConverter"), _T("Conversion. Can't open file %s to read"), _lpszFileName);
		throw CSystemException(::GetLastError());
	}

	DWORD dwFileSize = ::GetFileSize(hFile, NULL); // FIXME: check file size;

	boost::scoped_array<BYTE> pArray(new BYTE[dwFileSize]);

	DWORD dwBytesRead = 0;

	if(::ReadFile(hFile, pArray.get(), dwFileSize, &dwBytesRead, NULL) == FALSE)
	{
		pLog->LogString(LEVEL_INFO, _T("Conversion. Can't read contents of file %s"), _lpszFileName);
		throw CSystemException(::GetLastError());
	}

	hFile.Close();
	boost::shared_array<BYTE> result;
	int dwResultSize = 0;
	try
	{
		::ConvertWave(pArray.get(), dwBytesRead, result, dwResultSize, fmt);
	}
	catch(std::exception& ex)
	{
		CSystemException excp(::GetLastError());
		CString strError;
		strError.Format(_T("ConvertWave failed. %s"), ex.what());

		pLog->LogString(LEVEL_INFO, strError);
		EX_THROW(CConversionException, strError);
	}
	catch(...)
	{
		CString strError;
		strError.Format(_T("Conversion. Can't convert file %s. Codecs may not be installed"), _lpszFileName);

		pLog->LogString(LEVEL_INFO, strError);
		EX_THROW(CConversionException, strError);
	}

	hFile.Attach(::CreateFile(_lpszFileName, GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL));
	if(hFile == INVALID_HANDLE_VALUE)
	{
		throw CSystemException(::GetLastError());
	}

	if(::WriteFile(hFile, result.get(), dwResultSize, &dwBytesRead, NULL) == FALSE)
	{
		pLog->LogString(LEVEL_INFO, _T("Conversion. Can't write file %s."), _lpszFileName);
		throw CSystemException(::GetLastError());
	}

	pLog->LogString(LEVEL_INFO, _T("Conversion. Success. File %s."), _lpszFileName);
	return true;
}

//bool CConvertAction::IsWaveFile(LPCTSTR _lpszFileName)
//{
//	char waveHeader[12];
//	ZeroMemory(waveHeader, sizeof(waveHeader));
//
//	ATL::CHandle hFile;
//	hFile.Attach(::CreateFile(_lpszFileName, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL));
//	if(hFile == INVALID_HANDLE_VALUE)
//	{
//		return false;
//	}
//	
//	DWORD bytesRead = 0;
//
//	if(!::ReadFile(hFile, waveHeader, sizeof(waveHeader), &bytesRead, NULL))
//		return false;
//
//	if(bytesRead != 12)
//		return false;
//
//	return	waveHeader[0] == 'R' && waveHeader[1] == 'I' && waveHeader[2] == 'F' && waveHeader[3] == 'F' &&
//			waveHeader[8] == 'W' && waveHeader[9] == 'A' && waveHeader[10] == 'V' && waveHeader[11] == 'E';
//}
//
//void CConvertAction::AutoConvertFile(LPCTSTR _lpszFileName)
//{
//	singleton_auto_pointer<CSystemConfiguration> configuration;
//
//	try
//	{
//		if(CConvertAction::IsWaveFile(_lpszFileName) && configuration->AutoConvert)
//		{
//			CConvertAction::Convert(_lpszFileName, WAVE_FORMAT_PCM, configuration->BitsPerSample, configuration->SamplesPerSecond);
//		}
//	}	
//	catch(...)
//	{
//	}
//}
//
//void CConvertAction::ReadHeader(LPCTSTR _lpszFileName, WAVEFORMATEX& _format)
//{
//	CBinaryFile bf;
//	bf.Open(_lpszFileName, GENERIC_READ);
//
//	char szTmp[10];
//	WAVEFORMATEX pcmWaveFormat;
//	ZeroMemory(szTmp, 10 * sizeof(char));
//	bf.Read(szTmp, 4 * sizeof(char)) ;
//
//	DWORD dwFileSize/* = m_buffer.GetNumSamples() * m_pcmWaveFormat.nBlockAlign + 36*/ ;
//	bf.Read(&dwFileSize, sizeof(dwFileSize)) ;
//	ZeroMemory(szTmp, 10 * sizeof(char));
//	bf.Read(szTmp, 8 * sizeof(char)) ;
//	DWORD dwFmtSize /*= 16L*/;
//	bf.Read(&dwFmtSize, sizeof(dwFmtSize)) ;
//	bf.Read(&_format.wFormatTag, sizeof(pcmWaveFormat.wFormatTag)) ;
//	bf.Read(&_format.nChannels, sizeof(pcmWaveFormat.nChannels)) ;
//	bf.Read(&_format.nSamplesPerSec, sizeof(pcmWaveFormat.nSamplesPerSec)) ;
//	bf.Read(&_format.nAvgBytesPerSec, sizeof(pcmWaveFormat.nAvgBytesPerSec)) ;
//	bf.Read(&_format.nBlockAlign, sizeof(pcmWaveFormat.nBlockAlign)) ;
//	bf.Read(&_format.wBitsPerSample, sizeof(pcmWaveFormat.wBitsPerSample)) ;
//}
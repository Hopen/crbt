// ADODatabase.cpp: implementation of the CADODatabase class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ADODatabase.h"

//////////////////////////////////////////////////////////////////////
// CADODatabase
//////////////////////////////////////////////////////////////////////

void CADODatabase::Msg(LPCTSTR sMsg)
{
	::MessageBox(NULL, sMsg, _T("CADODatabase"), MB_ICONSTOP|MB_OK);
}

CADODatabase::CADODatabase()
{   
   try {
		m_pIConn = NULL;
		TESTHR(CoInitialize(NULL));
		TESTHR(m_pIConn.CreateInstance(__uuidof(Connection)));
   }
   catch(_com_error e) {
      Msg(e.ErrorMessage());
   }
   catch(...) {
      Msg(_T("Unable to initialize database connection."));
   }
}

CADODatabase::~CADODatabase()
{
	try {
	  if (IsOpen())
		 Close();

	  m_pIConn = NULL;

	  CoUninitialize();
	}
	catch(...) {
	  _ASSERT(FALSE);
	}
}

bool CADODatabase::IsOpen()
{
	if(m_pIConn)
		return m_pIConn->GetState() != adStateClosed;

	return false;
}

bool CADODatabase::Open(LPCSTR strDB, LPCSTR strUID, LPCSTR strPWD, LPCSTR strProvider /*= "MSDASQL"*/, enum CursorLocationEnum curLocation /*= adUseClient*/)
{
	//try {
		//m_pIConn->Provider = _bstr_t(strProvider);

		TESTHR(m_pIConn->Open(_bstr_t(strDB), _bstr_t(strUID), _bstr_t(strPWD), 0));

//		m_pIConn->CursorLocation = adUseClient;
//		m_pIConn->CursorLocation = adUseServer;	//for getting Recordsets
		m_pIConn->CursorLocation = curLocation;
	//}
	//catch(...) {
	//	CString sCon;
	//	sCon.Format(L"Error opening DB: \nDB       = %s, \nUID       = %s, \nPWD      = %s, \nProvider = %s", strDB, strUID, strPWD, strProvider);
	//	Msg(sCon);
	//	// Msg("Unable to open database connection.");
	//	return false;
	//}

	return true;
}

bool CADODatabase::Perform(DbActions doAction)
{
   try {
      switch (doAction) {
         case doClose:
            TESTHR(m_pIConn->Close());
            break;
         case doBeginTrans:
            m_pIConn->BeginTrans();
            break;
         case doCommitTrans:
            m_pIConn->CommitTrans();
            break;
         case doRollbackTrans:
            m_pIConn->RollbackTrans();
            break;       
      };
      return TRUE;
   }
   catch(_com_error e) {
      _ASSERT(FALSE);
      return FALSE;
   }
   catch(...) {
      _ASSERT(FALSE);
      return FALSE;
   }
}

bool CADODatabase::Execute(LPCTSTR lpstrExec)
{
	//try
	//{
		m_pIConn->Execute(_bstr_t(lpstrExec), NULL, adExecuteNoRecords);
		return true;
	//}
	//catch(_com_error &e)
	//{
	//	dump_com_error(e);
	//	return false;	
	//}
}

//////////////////////////////////////////////////////////////////////
// CADOStoredProc
//////////////////////////////////////////////////////////////////////

CADOStoredProc::CADOStoredProc()
{
	m_bDbPrivateConnection = false;
	m_pDb = NULL;
}

CADOStoredProc::CADOStoredProc(LPCTSTR spName, CADODatabase* pDb)
{
	m_bDbPrivateConnection = false;
	Initialize(spName, pDb);
}

CADOStoredProc::~CADOStoredProc()
{
	m_pCommand = NULL;

	if( m_pRS ) {
		if( m_pRS->State == adStateOpen )
			m_pRS->Close();

		m_pRS = NULL;
	}

	if (m_bDbPrivateConnection && m_pDb) {
		m_pDb->Close();
		delete m_pDb;
		m_pDb = NULL;
	}
}

bool CADOStoredProc::Initialize(LPCTSTR spName, LPCSTR strDB, LPCSTR strUID, LPCSTR strPWD)
{
	if (m_bDbPrivateConnection && m_pDb) {
		m_pDb->Close();
	}

	CADODatabase *pDb = new (CADODatabase);

	if (pDb) {
		m_bDbPrivateConnection = true;
		if (pDb->Open(strDB, strUID, strPWD, "", adUseClient)) {
			return Initialize(spName, pDb);
		}
	}

	return false;
}

bool CADOStoredProc::Initialize(LPCTSTR spName, CADODatabase* pDb)
{
	//ASSERT(pDb);

	try {
		if (m_bDbPrivateConnection && m_pDb) {
			m_pDb->Close();
		}

		m_pCommand = NULL;
		m_pDb = pDb;

		if (m_pRS) {
			if( m_pRS->State == adStateOpen ) {
				m_pRS->Close();
			}

			m_pRS = NULL;
		}

		TESTHR(m_pCommand.CreateInstance(__uuidof(Command)));

		m_pCommand->ActiveConnection = m_pDb->m_pIConn;
		m_pCommand->CommandText = spName;
		m_pCommand->CommandType = adCmdStoredProc;

		return true;
	}
	catch(...) {
	}
	return false;
}

int CADOStoredProc::GetRcParam()
{
	_variant_t vVal;

	if (GetParameter(_T("RETURN_VALUE"), &vVal))
		return vVal.lVal;
	else
		return -1;
}

bool CADOStoredProc::AdRcParam()
{
	return AdParam(_T("RETURN_VALUE"), adInteger, adParamReturnValue, 4);
}

bool CADOStoredProc::AdParam(LPCTSTR sParName, enum /*ADODB::*/DataTypeEnum iParType, enum /*ADODB::*/ParameterDirectionEnum iDirection, short iParamLen)
{
	_variant_t vtNull;
	_ParameterPtr p = m_pCommand->CreateParameter(sParName, iParType, iDirection, iParamLen, vtNull);
	if (NULL != p) {
		return (S_OK == m_pCommand->Parameters->Append(p));
	}

	return false;
}

bool CADOStoredProc::SetParam(LPCTSTR sParName, const _variant_t &vVal)
{
	_ParameterPtr p = m_pCommand->Parameters->GetItem(sParName);
	if (NULL != p) {
		return (S_OK == p->put_Value(vVal));
	}

	return false;
}

bool CADOStoredProc::GetParam(LPCTSTR sParName, _variant_t &vVal)
{
	return  (S_OK == GetParameter(_variant_t(sParName), &vVal));
}

bool CADOStoredProc::GetParameter(const _variant_t& vtParameterName, _variant_t * pvtValue)
{
	_ParameterPtr p = m_pCommand->Parameters->GetItem(vtParameterName);
	if (NULL != p) {
		return (S_OK == p->get_Value (pvtValue));
	}

	return false;
}

bool CADOStoredProc::Execute()
{
   //try {
		m_pRS = m_pCommand->Execute(NULL, NULL, adCmdStoredProc);
		return true;
//   }
//   catch(_com_error e) {
//		AfxMessageBox(e.Description());
////		AfxMessageBox(e.ErrorMessage());
//		return false;
//   }
//   catch(...) {
//		return false;
//   }
}

//bool CADOStoredProc::Execute(LPCTSTR spCommand)
//{
//	//m_pCommand->CommandText = _bstr_t(spCommand);
//	//m_pCommand->Execute(_bstr_t(spCommand), NULL, adExecuteNoRecords);
//	//m_pRS = m_pCommand->Execute(NULL, NULL, adCmdText);
//	return true;
//}


bool CADOStoredProc::Connect(LPCTSTR _proc_name, LPCSTR _connection_str, CADODatabase *pDB/* = NULL */)
{
	if (pDB) 
	{
		// Use existing connection
		if (Initialize( _proc_name, pDB))
			return AdAllParam();
	}
	else 
	{
		// Open new connection
		if (Initialize( _proc_name, _connection_str, "", ""))
			return AdAllParam();
	}
	return false;
}

//bool CADOStoredProc::AdAllParam()
//{
//	return false;
//}


//////////////////////////////////////////////////////////////////////
// CADORecordset
//////////////////////////////////////////////////////////////////////

CADORecordset::CADORecordset(CADODatabase* pDb, int nCacheSize) : m_pDb(pDb)
{
   try {
//      TESTHR(CoInitialize(NULL));
      TESTHR(m_pRS.CreateInstance(__uuidof(Recordset)));     
      if (nCacheSize != -1)
         m_pRS->CacheSize = nCacheSize;
   }
   catch(...) {
   }
}

CADORecordset::CADORecordset()
{
}

CADORecordset::~CADORecordset()
{
	//try {
		if (IsOpen()) {
			m_pRS->Close();
			m_pRS->Release();
			m_pRS = NULL;
		}

	//      CoUninitialize();
	//}
	//catch(...) {
	//	ASSERT(TRUE);
	//}
}

bool CADORecordset::Initialize(CADODatabase* pDb, int nCacheSize /*= -1*/)
{
//	ASSERT(pDb);

	m_pDb = pDb;
	//try {
	  TESTHR(m_pRS.CreateInstance(__uuidof(Recordset)));     
	  if (nCacheSize != -1)
		 m_pRS->CacheSize = nCacheSize;
	//}
	//catch(...) {
	//	return false;
	//}
	return true;
}

bool CADORecordset::Open(CString strSQL)
{
	try {     
	  TESTHR(m_pRS->Open(_bstr_t(strSQL), m_pDb->m_pIConn.GetInterfacePtr(), adOpenDynamic, adLockOptimistic, adCmdText));
	}
	catch(_com_error e) {
		//AfxMessageBox(e.ErrorMessage());
		return false;
	}
	catch(...) {
		//AfxMessageBox("Unable to open recordset.");
		return false;
	}

	return true;
}

long CADORecordset::GetRecordCount()
{
	DWORD nRows = 0;

	if (IsOpen()) {	
		nRows = m_pRS->GetRecordCount();

		if (nRows == -1) {
			nRows = 0;
			if (m_pRS->EndOfFile != VARIANT_TRUE)
				m_pRS->MoveFirst();

			while (m_pRS->EndOfFile != VARIANT_TRUE) {
				nRows++;
				m_pRS->MoveNext();
			}

			if(nRows > 0)
				m_pRS->MoveFirst();
		}
	}

	return (long)nRows;
}

bool CADORecordset::Perform(Actions doAction)
{
   try {
      switch (doAction) {

         case doMoveNext:
            TESTHR(m_pRS->MoveNext());
            break;

         case doMoveFirst:
            TESTHR(m_pRS->MoveFirst());
            break;

         case doMoveLast:
            TESTHR(m_pRS->MoveLast());
            break;

         case doMovePrevious:
            TESTHR(m_pRS->MovePrevious());
            break;
/*
         case doAddNew:
            if (m_pPicRS && m_pAdoRecordBinding)
            {
               TESTHR(m_pPicRS->AddNew(m_pAdoRecordBinding));
            }
            else
               TESTHR(m_pRS->AddNew());
            break;
         case doUpdate:
            if (m_pPicRS && m_pAdoRecordBinding)
               TESTHR(m_pPicRS->Update(m_pAdoRecordBinding));
            else
               TESTHR(m_pRS->Update());
            break;
*/
         case doDelete:
            TESTHR(m_pRS->Delete(adAffectCurrent));
            break;
      };
   }
   catch(_com_error e) {
     // AfxMessageBox(e.ErrorMessage());
     // AfxMessageBox(e.Source());
     // AfxMessageBox(e.Description());
      return false;
   }
   catch(...) {
		::MessageBox(NULL, _T("Unable to open database connection."), _T("CADORecordset"), MB_ICONSTOP|MB_OK);
		return false;
   }  
   return true;
}

bool CADORecordset::GetFieldValue(LPCTSTR lpFieldName, _variant_t& vtValue)
{
	try {
		vtValue = m_pRS->Fields->GetItem(lpFieldName)->Value;
		return true;
	}
	catch(...) {
		return false;
	}
}

bool CADORecordset::GetFieldValue(LPCTSTR lpFieldName, CString& sValue)
{
	_variant_t vtValue;
	if (GetFieldValue(lpFieldName, vtValue)) {
		sValue = vtValue.bstrVal;
		return true;
	}
	return false;
}

bool CADORecordset::GetFieldValue(short nIndex, _variant_t& vtValue)
{
	_variant_t vtIndex(nIndex);

	try {
		vtValue = m_pRS->Fields->GetItem(vtIndex)->Value;
		return true;
	}
	catch(...) {
		return false;
	}
}

bool CADORecordset::GetFieldValue(short nIndex, CString& sValue)
{
	_variant_t vtIndex(nIndex);

	try {
		_variant_t vtValue = m_pRS->Fields->GetItem(vtIndex)->Value;

		if (vtValue.vt == VT_NULL) {
			sValue.Empty();
		}
		else {
			vtValue.ChangeType(VT_BSTR);
			sValue = vtValue.bstrVal;
			sValue.TrimRight();
		}

		return true;
	}
	catch(...) {
		return false;
	}
}

CString CADORecordset::GetFieldName(short nIndex)
{
	CString str;
	try {
		_variant_t vtIndex(nIndex);
		str = (LPCSTR) m_pRS->Fields->GetItem(vtIndex)->Name;
	}
	catch(...) {
	}
	return str;
}

long CADORecordset::GetFieldType(short nIndex)
{
	long lRet;
	/*ADODB::*/PropertiesPtr Properties;

	try {
		_variant_t vtIndex(nIndex);

		lRet = m_pRS->Fields->GetItem(vtIndex)->Type;
	}
	catch(...) {
	}

	return lRet;
}

CString CADORecordset::GetFieldLen(short nIndex)
{
	CString str;

	try {
		_variant_t vtIndex(nIndex);
		str.Format(_T("%d"), m_pRS->Fields->GetItem(vtIndex)->GetDefinedSize());
	}
	catch(...) {
	}

	return str;
}
